package wtf.indivisible.utils.json;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

import java.util.HashMap;
import java.util.Map;

@JsonRootName(value = "test-pojo")
public class JsonPojo implements Jsonable<JsonPojo> {

    // primitive boolean
    public static final String BOOL_KEY = "test-bool";
    public static final boolean BOOL_DEFAULT = true;
    @JsonProperty(BOOL_KEY)
    private boolean boolValue;

    // class Boolean
    public static final String BOOLEAN_KEY = "test-boolean";
    public static final Boolean BOOLEAN_DEFAULT = Boolean.TRUE;
    @JsonProperty(BOOLEAN_KEY)
    private Boolean booleanValue;

    // primitive int
    public static final String INT_KEY = "test-int";
    public static final int INT_DEFAULT = 2;
    @JsonProperty(INT_KEY)
    private int intValue;

    // class Integer
    public static final String INTEGER_KEY = "test-integer";
    public static final Integer INTEGER_DEFAULT = 4;
    @JsonProperty(INTEGER_KEY)
    private Integer integerValue;

    // String
    public static final String STRING_KEY = "test-string";
    public static final String STRING_DEFAULT = "string default";
    @JsonProperty(STRING_KEY)
    private String strValue;

    // Map<String, String>
    public static final String MAP_KEY = "test-map";
    public static final String MAP_PAIR_KEY = "map-key";
    public static final String MAP_PAIR_VALUE = "map value";
    @JsonProperty(MAP_KEY)
    private Map<String, String> strMap;

    //========================================================//
    //  NON ANNOTATED FIELDS SHOULD BE IGNORED

    private boolean IGNORE_BOOL = true;
    private Boolean IGNORE_BOOLEAN = Boolean.FALSE;
    private int ignore_int = 123;
    private Integer ignore_integer = Integer.valueOf(321);
    private String ignoreString = "ignore IGNORE";


    //========================================================//
    //  init
    //========================================================//

    public JsonPojo() {
        boolValue = false;
        booleanValue = null;
        intValue = 0;
        integerValue = null;
        strValue = null;
        strMap = null;
    }


    //========================================================//
    //  checks
    //========================================================//

    public void setToDefault() {
        setBoolValue(BOOL_DEFAULT);
        setBooleanValue(BOOLEAN_DEFAULT);
        setIntValue(INT_DEFAULT);
        setIntegerValue(INTEGER_DEFAULT);
        setStrValue(STRING_DEFAULT);

        Map<String, String> defStrMap = new HashMap<>();
        defStrMap.put(MAP_PAIR_KEY, MAP_PAIR_VALUE);
        setStrMap(defStrMap);
    }

    public boolean isInitState() {
        return !boolValue
                && booleanValue == null
                && intValue == 0
                && integerValue == null
                && strValue == null
                && strMap == null;
    }

    public boolean isDefaultState() {
        return BOOL_DEFAULT == getBoolValue()
                && BOOLEAN_DEFAULT.equals(getBooleanValue())
                && INT_DEFAULT == getIntValue()
                && INTEGER_DEFAULT.equals(getIntegerValue())
                && STRING_DEFAULT.equals(getStrValue())

                && getStrMap() != null
                && getStrMap().keySet().size() == 1
                && getStrMap().containsKey(MAP_PAIR_KEY)
                && MAP_PAIR_VALUE.equals(getStrMap().get(MAP_PAIR_KEY));
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (!(o instanceof JsonPojo)) return false;

        final JsonPojo jsonPojo = (JsonPojo) o;

        if (getBoolValue() != jsonPojo.getBoolValue()) return false;
        if (getIntValue() != jsonPojo.getIntValue()) return false;
        if (getBooleanValue() != null ? !getBooleanValue().equals(jsonPojo.getBooleanValue()) : jsonPojo
                .getBooleanValue() != null)
            return false;
        if (getIntegerValue() != null ? !getIntegerValue().equals(jsonPojo.getIntegerValue()) : jsonPojo
                .getIntegerValue() != null)
            return false;
        if (getStrValue() != null ? !getStrValue().equals(jsonPojo.getStrValue()) : jsonPojo.getStrValue() != null)
            return false;
        return getStrMap() != null ? getStrMap().equals(jsonPojo.getStrMap()) : jsonPojo.getStrMap() == null;

    }

    @Override
    public int hashCode() {
        int result = (getBoolValue() ? 1 : 0);
        result = 31 * result + (getBooleanValue() != null ? getBooleanValue().hashCode() : 0);
        result = 31 * result + getIntValue();
        result = 31 * result + (getIntegerValue() != null ? getIntegerValue().hashCode() : 0);
        result = 31 * result + (getStrValue() != null ? getStrValue().hashCode() : 0);
        result = 31 * result + (getStrMap() != null ? getStrMap().hashCode() : 0);
        return result;
    }

    //========================================================//
    //  get & sets
    //========================================================//


    public boolean getBoolValue() {
        return boolValue;
    }

    public void setBoolValue(final boolean boolValue) {
        this.boolValue = boolValue;
    }

    public Boolean getBooleanValue() {
        return booleanValue;
    }

    public void setBooleanValue(final Boolean booleanValue) {
        this.booleanValue = booleanValue;
    }

    public int getIntValue() {
        return intValue;
    }

    public void setIntValue(final int intValue) {
        this.intValue = intValue;
    }

    public Integer getIntegerValue() {
        return integerValue;
    }

    public void setIntegerValue(final Integer integerValue) {
        this.integerValue = integerValue;
    }

    public String getStrValue() {
        return strValue;
    }

    public void setStrValue(final String strValue) {
        this.strValue = strValue;
    }

    public Map<String, String> getStrMap() {
        return strMap;
    }

    public void setStrMap(final Map<String, String> strMap) {
        this.strMap = strMap;
    }

    //========================================================//
    //  still want to have gets & sets for ignored fields


    public boolean isIGNORE_BOOL() {
        return IGNORE_BOOL;
    }

    public void setIGNORE_BOOL(final boolean IGNORE_BOOL) {
        this.IGNORE_BOOL = IGNORE_BOOL;
    }

    public Boolean getIGNORE_BOOLEAN() {
        return IGNORE_BOOLEAN;
    }

    public void setIGNORE_BOOLEAN(final Boolean IGNORE_BOOLEAN) {
        this.IGNORE_BOOLEAN = IGNORE_BOOLEAN;
    }

    public int getIgnore_int() {
        return ignore_int;
    }

    public void setIgnore_int(final int ignore_int) {
        this.ignore_int = ignore_int;
    }

    public Integer getIgnore_integer() {
        return ignore_integer;
    }

    public void setIgnore_integer(final Integer ignore_integer) {
        this.ignore_integer = ignore_integer;
    }

    public String getIgnoreString() {
        return ignoreString;
    }

    public void setIgnoreString(final String ignoreString) {
        this.ignoreString = ignoreString;
    }

}
