package wtf.indivisible.utils.json;


import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import wtf.indivisible.utils.files.FileUtils;
import wtf.indivisible.utils.nulls.Nulls;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class JsonTests {

    private static final Logger mLog = LoggerFactory.getLogger(JsonTests.class);


    private static final String FOLDER_TEST = "tests";
    private static final String FILENAME_INIT_STATE = "pojo-init.json";
    private static final String FILENAME_DEF_STATE = "pojo-def.json";

    @Rule
    public TemporaryFolder testRoot = new TemporaryFolder();


    @Test
    public void testPojoState() {
        JsonPojo pojo = new JsonPojo();
        assertTrue("json pojo not in init state after construction", pojo.isInitState());

        pojo.setToDefault();
        assertTrue("json pojo not in default state after setToDefault()", pojo.isDefaultState());
    }

    @Test
    public void initPojoToAndFromString() {
        JsonPojo pojo = new JsonPojo();
        assertTrue("constructed pojo [init] not in desired state to test", pojo.isInitState());

        String pojoJson = JsonMapper.toJsonString(pojo, false);
        mLog.debug("pojo [init] json: \n{}", pojoJson);
        assertNotNull("pojo [init] json string null", pojoJson);
        assertTrue("pojo [init] json empty", Nulls.notNullOrEmptyWithTrim(pojoJson));

        JsonPojo loadPojo = JsonMapper.fromJsonString(pojoJson, JsonPojo.class);
        assertNotNull("loaded pojo [init] from json null", loadPojo);
        assertTrue("loaded pojo [init] not in same state as persisted", loadPojo.isInitState());
    }

    @Test
    public void defaultPojoToAndFromString() {
        JsonPojo pojo = new JsonPojo();
        pojo.setToDefault();
        assertTrue("constructed pojo [def] not in desired state to test", pojo.isDefaultState());

        String pojoJson = JsonMapper.toJsonString(pojo, true);
        String stringableJson = pojo.asString();
        mLog.debug("pojo [def] json: \n{}", pojoJson);
        assertNotNull("pojo [def] json string null", pojoJson);
        assertTrue("pojo [def] json empty", Nulls.notNullOrEmptyWithTrim(pojoJson));
        assertNotNull("pojo [def] stringableJson string null", stringableJson);
        assertTrue("pojo [def] stringableJson empty", Nulls.notNullOrEmptyWithTrim(stringableJson));

        JsonPojo loadPojo = JsonMapper.fromJsonString(pojoJson, JsonPojo.class);
        assertNotNull("loaded pojo [def] from json null", loadPojo);
        assertTrue("loaded pojo [def] not in same state as persisted", loadPojo.isDefaultState());

        loadPojo = pojo.fromString(stringableJson);
        assertNotNull("loaded pojo [def] from json null", loadPojo);
        assertTrue("loaded pojo [def] not in same state as persisted", loadPojo.isDefaultState());
    }

    @Test
    public void testPrettyPrintVsCondensed() {
        JsonPojo pojo = new JsonPojo();

        String prettyJson = JsonMapper.toJsonString(pojo, true);
        String condensedJson = JsonMapper.toJsonString(pojo, false);

        assertTrue("Pretty print json invalid (null or empty)", Nulls.notNullOrEmptyWithTrim(prettyJson));
        assertTrue("Condensed json invalid (null or empty)", Nulls.notNullOrEmptyWithTrim(condensedJson));
        assertTrue("Pretty print should not be longer than condensed", prettyJson.length() > condensedJson.length());

        int prettyJsonLineReturns = prettyJson.split("\\n").length;
        int condensedJsonLineReturns = condensedJson.split("\\n").length;
        assertTrue("Pretty print json should have more newlines than condensed", prettyJsonLineReturns > condensedJsonLineReturns);

    }

    @Test
    public void testJsonFileIO() throws IOException {
        assertTrue("Test JSON temp folder root NOT writable: " + testRoot.getRoot().getAbsolutePath(),
                FileUtils.canWriteFolder(testRoot.getRoot()));

        JsonPojo outPojo = new JsonPojo();
        outPojo.setToDefault();
        File testFile = testRoot.newFile("PojoTest.json");
        JsonMapper.toJsonFile(testFile, outPojo);

        assertTrue("Test JSON file NOT created/readable: " + testFile.getAbsolutePath(),
                FileUtils.canReadFile(testFile));
        JsonPojo inPojo = JsonMapper.fromJsonFile(testFile, JsonPojo.class);
        assertNotNull("Test JsonPojo (inPojo) NULL on attempted read", inPojo);
        assertTrue("Test JsonPojo (inPojo) NOT in default state", inPojo.isDefaultState());
        assertEquals("Test JsonPojo(s) (outPojo/inPojo) NOT equal after file IO", outPojo, inPojo);

        // leave file for TemporaryFolder cleanup after tests
    }
}
