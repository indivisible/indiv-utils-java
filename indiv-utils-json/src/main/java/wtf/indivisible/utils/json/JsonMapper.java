package wtf.indivisible.utils.json;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import wtf.indivisible.utils.files.FileUtils;

import java.io.File;
import java.io.IOException;

public class JsonMapper {

        private static final Logger mLog = LoggerFactory.getLogger(JsonMapper.class);

        ////////////////////////////////////////////////////////////
        ////    static data
        ////////////////////////////////////////////////////////////

        //  create once, pass around, copy for edit
        private static ObjectMapper MASTER_MAPPER = null;
        private static final Object mapper_init_lock = new Object();

        // defaults
        private static final boolean DEFAULT_PRETTY_PRINT_TRANSPORT = false;
        private static final boolean DEFAULT_PRETTY_PRINT_FILE = true;


        ////////////////////////////////////////////////////////////
        ////    ObjectMapper
        ////////////////////////////////////////////////////////////

        private static ObjectMapper getDefaultMapper() {

            if (MASTER_MAPPER == null) {
                synchronized (mapper_init_lock) {
                    if (MASTER_MAPPER == null) {
                        ObjectMapper mapper = new ObjectMapper();

                        //========================================================//
                        //  field discovery

                        mapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.NONE);     // Disable auto detection
                        mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.NONE);   // Ignore NON annotated fields

                        //========================================================//
                        //  reading / deserialization

                        mapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);     // Square pegs can fit round holes
                        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);     // ignore extraneous properties
                        mapper.configure(DeserializationFeature.FAIL_ON_NUMBERS_FOR_ENUMS, true);       // Enforce explicit definitions

                        //========================================================//
                        //  writing / serialization

                        //mapper.configure(SerializationFeature.ORDER_MAP_ENTRIES_BY_KEYS, true);         // sorting for maps

                        MASTER_MAPPER = mapper;
                    }
                }
            }

            return MASTER_MAPPER;
        }

        public static ObjectMapper getEditableDefaultMapper() {
            return getDefaultMapper().copy();
        }


        ////////////////////////////////////////////////////////////
        ////    TO and from String
        ////////////////////////////////////////////////////////////

        /**
         * Loadable --> Json
         *
         * @param obj         implements {@link Jsonable}, an object with @JsonAnnotations on exposed fields
         * @param prettyPrint null is treated as {@value DEFAULT_PRETTY_PRINT_TRANSPORT}
         * @return null on error
         */
        public static String toJsonString(final Jsonable obj, final Boolean prettyPrint) {

            if (obj == null) {
                mLog.warn("Null object supplied, null returned");
                return null;
            }

            boolean doPrettyPrint = (prettyPrint == null) ? DEFAULT_PRETTY_PRINT_TRANSPORT : prettyPrint;
            try {
                if (doPrettyPrint) {
                    return getDefaultMapper().writerWithDefaultPrettyPrinter().writeValueAsString(obj);
                } else {
                    return getDefaultMapper().writeValueAsString(obj);
                }
            } catch (JsonProcessingException e) {
                mLog.error("JSON exception serializing a [{}]. Ex: {}", obj.getClass().getSimpleName(), e);
            } catch (NullPointerException e) {
                mLog.error("Base Json ObjectMapper null! Check its init/build.");
            }
            return null;
        }

        /**
         * Json --> Loadable
         * <p>
         * The class is asked for to avoid any inheritance confusions.
         * <p>
         * Always returns null if either input param is null (or empty for the String). <br/>
         * Also returns null on any IO, Json mapper, cast errors.
         *
         * @param inputJson json input string
         * @param clazz     the class to load and return
         * @param <T>       implements {@link Jsonable}, an object with @JsonAnnotations on exposed fields
         * @return null on error
         */
        public static <T extends Jsonable> T fromJsonString(final String inputJson, final Class<T> clazz) {

            if (!isMinimallyValidJson(inputJson)) {
                mLog.warn("Invalid Json. Must open with '{' and close with '}'");
                return null;
            }

            try {
                return getDefaultMapper().readValue(inputJson, clazz);
            } catch (JsonParseException | JsonMappingException e) {
                mLog.error("JSON parsing exception reading [{}] for [{}]. Ex = {}", inputJson.length(), clazz.getSimpleName(), e);
            } catch (IOException e) {
                mLog.error("IO Failure reading [{}] chars for [{}]. Ex = {}", inputJson.length(), clazz.getSimpleName(), e);
            } catch (NullPointerException e) {
                mLog.error("Base Json ObjectMapper null! Check its init/build.");
            }

            return null;

        }

        //========================================================//
        //  To and from File
        //========================================================//

        public static boolean toJsonFile(final File outputFile, final Jsonable obj, final boolean doPrettyPrint) {

            if (obj == null) {
                mLog.warn("Supplied object null.");
            } else if (outputFile == null) {
                mLog.warn("Supplied file null.");
            } else if (FileUtils.canWriteFolder(outputFile.getParentFile())) {
                try {
                    if (doPrettyPrint) {
                        getDefaultMapper().writerWithDefaultPrettyPrinter().writeValue(outputFile, obj);
                    } else {
                        //NB: Assumes DEFAULT state is prettyPrint=off
                        getDefaultMapper().writeValue(outputFile, obj);
                    }
                    return true;
                } catch (JsonGenerationException | JsonMappingException e) {
                    mLog.error("JSON exception writing [{}] for [{}]. Ex = {}", FileUtils.path(outputFile), obj.getClass().getSimpleName(), e);
                } catch (IOException e) {
                    mLog.error("IO Failure reading [{}] for [{}]. Ex = {}", FileUtils.path(outputFile), obj.getClass().getSimpleName(), e);
                }
            }

            // fallthrough fail
            return false;
        }

        public static boolean toJsonFile(final File outputFile, final Jsonable obj) {
            return toJsonFile(outputFile, obj, DEFAULT_PRETTY_PRINT_FILE);
        }

        public static <T extends Jsonable> T fromJsonFile(final File file, final Class<T> clazz) {

            if (!FileUtils.canReadFile(file)) {
                mLog.warn("Supplied file invalid or unreadable [{}]", FileUtils.path(file));
                return null;
            } else if (clazz == null) {
                mLog.warn("Supplied class null for file [{}]", FileUtils.path(file));
                return null;
            }

            try {
                return getDefaultMapper().readValue(file, clazz);
            } catch (JsonParseException | JsonMappingException e) {
                mLog.error("JSON parsing exception reading [{}] for [{}]. Ex = {}", FileUtils.path(file), clazz.getSimpleName(), e);
            } catch (IOException e) {
                mLog.error("IO Failure reading [{}] for [{}]. Ex = {}", FileUtils.path(file), clazz.getSimpleName(), e);
            }

            // fallthrough fail
            return null;
        }


        ////////////////////////////////////////////////////////////
        ////    validation
        ////////////////////////////////////////////////////////////

        /**
         * Trims then checks not empty,
         * starts with {@code '{'
         * and ends with {@code '}'
         *
         * @param jsonString
         * @return
         */
        public static boolean isMinimallyValidJson(final String jsonString) {
            if (jsonString == null) return false;
            else {
                String trimmedJson = jsonString.trim();
                return !trimmedJson.isEmpty()
                        && trimmedJson.startsWith("{")
                        && trimmedJson.endsWith("}");
            }
        }

    }
