package wtf.indivisible.utils.json;

import wtf.indivisible.utils.generics.Stringable;

/**
 * Interface for classes that can be (de)serialised via JSON.
 * <p>
 * Uses com.fasterxml.jackson Json annotations and handling.
 *
 * @param <T> Self (or parent) type for generics casting. Must implement Jsonable.
 * @see wtf.indivisible.utils.json.JsonMapper JsonMapper
 */
public interface Jsonable<T extends Jsonable<T>> extends Stringable<T> {

    //// Default Json implementation

    default String asJson() {
        return JsonMapper.toJsonString(this, false);
    }

    @SuppressWarnings("unchecked")
    default T fromJson(final String json) {
        return (T) JsonMapper.fromJsonString(json, this.getClass());
    }


    //// Stringable<>

    @Override
    default String asString() {
        return asJson();
    }

    @Override
    default T fromString(final String json) {
        return fromJson(json);
    }

}
