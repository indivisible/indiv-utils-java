package wtf.indivisible.utils.cfg;

import wtf.indivisible.utils.id.Identifiable;
import wtf.indivisible.utils.id.Ids;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class TestCfgGenerator {

    //TODO: config pom(s) for access to indiv-utils-common test jar during test phase (TestId)
    /* ASK: prob want to do for other modules too and always make generic test classes
            in indiv-utils-common test for use anywhere they're needed.
            Module/functionality specific needs should still remain in the test folder
            for the given module. */
    public static class Id implements Identifiable {

        private final String mId;

        Id() {
            mId = Ids.UUID.newId();
        }

        @Override
        public String getId() {
            return mId;
        }
    }

    private static final Random RAND = new Random();
    private static final int LIST_LENGTH = 3;


    public static TestCfg newTestCfg() {

        TestCfg cfg = new TestCfg();
        cfg.setBoolValue(RAND.nextBoolean());
        cfg.setIntValue(RAND.nextInt());
        cfg.setLongValue(RAND.nextLong());
        cfg.setFloatValue(RAND.nextFloat());
        cfg.setDoubleValue(RAND.nextDouble());
        cfg.setId(new Id());

        List<String> list = new ArrayList<>();
        list.add(cfg.getName());
        for (int i = 1; i < LIST_LENGTH; i++) {
            list.add(Long.toString(RAND.nextLong()));
        }
        cfg.setListValue(list);

        return cfg;
    }
}
