package wtf.indivisible.utils.cfg;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import wtf.indivisible.utils.cfg.configs.SimpleCfg;
import wtf.indivisible.utils.id.Identifiable;

import java.util.List;
import java.util.Objects;

@JsonRootName(TestCfg.CFG_NAME)
public class TestCfg extends SimpleCfg {

    public static final String CFG_NAME = "test-config";

    public static final String KEY_BOOLEAN = "bool-value";
    public static final String KEY_STRING = "string-value";
    public static final String KEY_INT = "int-value";
    public static final String KEY_LONG = "long-value";
    public static final String KEY_FLOAT = "float-value";
    public static final String KEY_DOUBLE = "double-value";
    public static final String KEY_LIST = "list-value";
    public static final String KEY_ID = "id-value";


    @JsonProperty(TestCfg.KEY_BOOLEAN)
    private Boolean mBoolValue;
    @JsonProperty(TestCfg.KEY_STRING)
    private String mStringValue;

    @JsonProperty(TestCfg.KEY_INT)
    private Integer mIntValue;
    @JsonProperty(TestCfg.KEY_LONG)
    private Long mLongValue;
    @JsonProperty(TestCfg.KEY_FLOAT)
    private Float mFloatValue;
    @JsonProperty(TestCfg.KEY_DOUBLE)
    private Double mDoubleValue;

    @JsonProperty(TestCfg.KEY_LIST)
    private List<String> mListValue;
    @JsonProperty(TestCfg.KEY_ID)
    private Identifiable mId;


    public TestCfg() {
        super(TestCfg.CFG_NAME);
    }


    public Boolean getBoolValue() {
        return mBoolValue;
    }

    public void setBoolValue(final Boolean boolValue) {
        mBoolValue = boolValue;
    }

    public String getStringValue() {
        return mStringValue;
    }

    public void setStringValue(final String stringValue) {
        mStringValue = stringValue;
    }

    public Integer getIntValue() {
        return mIntValue;
    }

    public void setIntValue(final Integer intValue) {
        mIntValue = intValue;
    }

    public Long getLongValue() {
        return mLongValue;
    }

    public void setLongValue(final Long longValue) {
        mLongValue = longValue;
    }

    public Float getFloatValue() {
        return mFloatValue;
    }

    public void setFloatValue(final Float floatValue) {
        mFloatValue = floatValue;
    }

    public Double getDoubleValue() {
        return mDoubleValue;
    }

    public void setDoubleValue(final Double doubleValue) {
        mDoubleValue = doubleValue;
    }

    public List<String> getListValue() {
        return mListValue;
    }

    public void setListValue(final List<String> listValue) {
        mListValue = listValue;
    }

    public Identifiable getId() {
        return mId;
    }

    public void setId(final Identifiable id) {
        mId = id;
    }


    ////////////////////////////////////////////////////////////
    ////  equals
    ////////////////////////////////////////////////////////////


    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final TestCfg testCfg = (TestCfg) o;
        return Objects.equals(getBoolValue(), testCfg.getBoolValue()) &&
               Objects.equals(getStringValue(), testCfg.getStringValue()) &&
               Objects.equals(getIntValue(), testCfg.getIntValue()) &&
               Objects.equals(getLongValue(), testCfg.getLongValue()) &&
               Objects.equals(getFloatValue(), testCfg.getFloatValue()) &&
               Objects.equals(getDoubleValue(), testCfg.getDoubleValue()) &&
               Objects.equals(getListValue(), testCfg.getListValue()) &&
               Objects.equals(getId(), testCfg.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getBoolValue(), getStringValue(), getIntValue(), getLongValue(), getFloatValue(), getDoubleValue(), getListValue(), getId());
    }
}
