package wtf.indivisible.utils.ex;

import wtf.indivisible.ex.IndivRuntimeException;

public class ConfigException extends IndivRuntimeException {

    public ConfigException(final String message) {
        super(message);
    }

    public ConfigException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
