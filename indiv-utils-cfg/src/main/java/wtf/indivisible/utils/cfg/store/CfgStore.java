package wtf.indivisible.utils.cfg.store;

import wtf.indivisible.utils.cfg.Cfg;
import wtf.indivisible.utils.cfg.configs.VersionCfg;

public interface CfgStore<T extends Cfg> {

    /**
     * Test for existence of a specific version of a {@link Cfg}
     *
     * @param cfgName    identifying name of the {@link Cfg}
     * @param cfgVersion matched if supplied, if 'null' looks for a "default" {@link Cfg} version
     * @return true if found, false if not
     */
    boolean exists(final String cfgName, final String cfgVersion);

    /**
     * Test for existence of an unversioned (or default) {@link Cfg} instance
     *
     * @param cfgName identifying name of the {@link Cfg}
     * @return true if found, false if not
     * @see CfgStore#exists(String, String)
     */
    default boolean exists(final String cfgName) {
        return exists(cfgName, null);
    }


    /**
     * Load the specific versioned {@link Cfg} or {@code null} if not found or other error.
     *
     * @param cfgName
     * @param cfgVersion
     * @return
     */
    T load(final String cfgName, final String cfgVersion);

    /**
     * Load the unversioned or default instance of the named {@link Cfg}
     * or {@code null} if not found or error.
     *
     * @param cfgName
     * @return
     * @see CfgStore#load(String, String)
     */
    default T load(final String cfgName) {
        return load(cfgName, null);
    }


    /**
     * Persist the supplied {@link Cfg} under the given name and version descriptors.
     *
     * @param cfgName    the String identifying name for the Cfg
     * @param cfgVersion the String version to persist the Cfg under
     * @param cfg        the Cfg to persist
     * @return true if successfully persisted, false otherwise
     */
    boolean save(final String cfgName, final String cfgVersion, final T cfg);

    /**
     * Persist the supplied (unversioned) {@link Cfg} under the given name
     *
     * @param cfgName the String identifying name for the Cfg
     * @param cfg     the Cfg to persist
     * @return true if successfully persisted, false otherwise
     * @see CfgStore#save(String, String, Cfg)
     */
    default boolean save(final String cfgName, final T cfg) {
        return save(cfgName, null, cfg);
    }

    /**
     * Persist the supplied {@link Cfg} using its own defined name and/or version.
     *
     * @param cfg the Cfg to persist
     * @return true if successfully persisted, false otherwise
     * @see CfgStore#save(String, Cfg)
     * @see CfgStore#save(String, String, Cfg)
     */
    default boolean save(final T cfg) {
        if (cfg == null) {
            return false;
        } else if (cfg instanceof VersionCfg) {
            return save(cfg.getName(), ((VersionCfg) cfg).getVersion(), cfg);
        } else {
            return save(cfg.getName(), cfg);
        }
    }

}
