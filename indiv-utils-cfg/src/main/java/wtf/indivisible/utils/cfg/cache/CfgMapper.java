package wtf.indivisible.utils.cfg.cache;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import wtf.indivisible.utils.cfg.Cfg;
import wtf.indivisible.utils.ex.ConfigException;
import wtf.indivisible.utils.nulls.NullSafe;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Class to hold mappings of Configs to their Class to aid in (de)serialisation.
 * <p>
 * To be able to (de)serialise configs, we need to know the class structures ahead of time
 * so all supported Cfg/Configs MUST be pre-registered before first use.
 */
public class CfgMapper {

    private static final Logger LOG = LoggerFactory.getLogger(CfgMapper.class);

    /**
     * Internal mapping of (String) name to Cfg classes
     */
    private static Map<String, Class<? extends Cfg>> mNameMap;

    static {
        mNameMap = new ConcurrentHashMap<>();
    }


    //// query

    /**
     * Query whether the given name has already been registered.
     *
     * @param cfgName
     * @return
     */
    public static boolean hasName(final String cfgName) {
        return mNameMap.containsKey(cfgName);
    }

    /**
     * Query whether the given class has already been registered.
     *
     * @param cfgClass
     * @param <T>
     * @return
     */
    public static <T extends Cfg> boolean hasClass(final Class<T> cfgClass) {
        return mNameMap.containsValue(cfgClass);
    }


    //// access

    /**
     * Retrieve the registered class with the supplied cfgName as key.
     *
     * @param cfgName
     * @return
     */
    public static Class<? extends Cfg> getCfgClass(final String cfgName) {
        return mNameMap.get(cfgName);
    }


    /**
     * Register a Cfg class against the supplied cfgName
     *
     * @param cfgName
     * @param cfgClass
     * @param <T>
     * @return
     * @throws ConfigException
     */
    public static <T extends Cfg> boolean registerCfg(final String cfgName, final Class<T> cfgClass) throws ConfigException {
        String cleanName = NullSafe.trim(cfgName, true);
        if (cleanName == null) {
            throw new ConfigException("Cannot register Cfg (bad name)");
        } else if (cfgClass == null) {
            throw new ConfigException("Cannot register Cfg (bad class)");
        } else if (hasName(cleanName)) {
            LOG.warn("ConfigMapper already has an entry for " + cleanName);
            return false;
        } else {
            mNameMap.put(cleanName, cfgClass);
            return true;
        }
    }

    /**
     * Register a Cfg class using the provided Cfg instance.
     * The state of the Cfg is not important only the exposed name ({@link Cfg#getName()}) and its class
     *
     * @param cfg
     * @param <T>
     * @return
     * @throws ConfigException
     */
    public static <T extends Cfg> boolean registerCfg(final T cfg) throws ConfigException {
        if (cfg == null) {
            throw new ConfigException("Cannot register a NULL Cfg");
        } else {
            return registerCfg(cfg.getName(), cfg.getClass());
        }
    }

}
