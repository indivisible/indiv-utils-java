package wtf.indivisible.utils.cfg.cache;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import wtf.indivisible.utils.cfg.Cfg;
import wtf.indivisible.utils.ex.ConfigException;
import wtf.indivisible.utils.nulls.Nulls;

import java.util.HashMap;
import java.util.Map;

public class CfgCache {

    private static final Logger LOG = LoggerFactory.getLogger(CfgCache.class);

    private Map<String, Cfg> mLoadedConfigs;
    private Map<String, Cfg> mModifiedConfigs;


    public CfgCache() {
        mLoadedConfigs = new HashMap<>();
        mModifiedConfigs = new HashMap<>();
    }


    //// access

    public Cfg getLoaded(final String cfgName) {
        return mLoadedConfigs.get(cfgName);
    }

    public Cfg getLatest(final String cfgName) {
        if (mModifiedConfigs.containsKey(cfgName)) {
            return mModifiedConfigs.get(cfgName);
        } else {
            return getLoaded(cfgName);
        }
    }


    public boolean put(final String cfgName, final Cfg cfg) throws ConfigException {
        if (Nulls.isNullOrEmptyWithTrim(cfgName)) {
            throw new ConfigException("Invalid cfgName supplied (required non-null, non-empty)");
        } else if (mLoadedConfigs.containsKey(cfgName)) {
            LOG.warn("ConfigStore already contains a Cfg with name: " + cfg.getName());
            return false;
        } else {
            mLoadedConfigs.put(cfgName, cfg);
            return true;
        }
    }

    public boolean put(final Cfg cfg) throws ConfigException {
        if (cfg == null) {
            throw new ConfigException("Cannot store NULL Cfg");
        } else {
            return put(cfg.getName(), cfg);
        }
    }


}
