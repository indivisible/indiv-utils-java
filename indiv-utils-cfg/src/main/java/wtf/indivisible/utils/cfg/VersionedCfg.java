package wtf.indivisible.utils.cfg;

import wtf.indivisible.utils.generics.Versioned;

/**
 * Extension to {@link Cfg} including version information (String)
 */
public interface VersionedCfg extends Cfg, Versioned<String> {


}
