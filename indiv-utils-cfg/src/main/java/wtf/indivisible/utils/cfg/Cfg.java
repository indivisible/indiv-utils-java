package wtf.indivisible.utils.cfg;

import wtf.indivisible.utils.generics.Nameable;
import wtf.indivisible.utils.json.Jsonable;

/**
 * Basic configuration interface.
 * <p>
 * Requires a String name for the config via the implementation of {@link Nameable#getName()}
 * Name expected to be (usage/namespace) unique to avoid Cfg clashes, invalid reads or accidental overwriting.
 * <p>
 * The convention is for the name to be defined as {@code public static final String CFG_NAME}. While not required
 * (since enforcing static contracts is neigh impossible) all internal/base classes expose their names with this
 * static variable for ease of access in static contexts.
 */
public interface Cfg extends Nameable, Jsonable {

    /**
     * Overrides method {@link Nameable#getName()} for exposing a String name that can be used to identify the
     * {@link Cfg} instance.
     *
     * The value returned SHOULD be an alpha-numeric string, hyphens for spacers and SHOULD NOT contain whitespace. <br>
     * Regex: '{@code ^[a-zA-Z0-9]?+[-a-zA-Z0-9].?[a-zA-Z0-9]?+$}'
     *
     * @return
     */
    String getName();


}
