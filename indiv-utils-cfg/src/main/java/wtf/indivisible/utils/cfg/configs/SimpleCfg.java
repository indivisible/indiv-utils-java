package wtf.indivisible.utils.cfg.configs;

import com.fasterxml.jackson.annotation.JsonProperty;
import wtf.indivisible.utils.cfg.Cfg;
import wtf.indivisible.utils.nulls.NullSafe;

/**
 * Base implementation of {@link Cfg}.
 */
public abstract class SimpleCfg implements Cfg {

    //// statics
    public static final String DEFAULT_NAME = "config";


    //// meta
    @JsonProperty("name")
    private final String mConfigName;


    //// init
    protected SimpleCfg(final String configName) {
        String cleanedName = NullSafe.trim(configName, true);
        mConfigName = (cleanedName == null) ? DEFAULT_NAME : cleanedName;
    }


    //// Cfg implements

    @Override
    public String getName() {
        return mConfigName;
    }


}
