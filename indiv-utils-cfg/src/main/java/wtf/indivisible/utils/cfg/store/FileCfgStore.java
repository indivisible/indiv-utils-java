package wtf.indivisible.utils.cfg.store;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import wtf.indivisible.utils.cfg.Cfg;
import wtf.indivisible.utils.cfg.cache.CfgMapper;
import wtf.indivisible.utils.cfg.configs.VersionCfg;
import wtf.indivisible.utils.ex.ConfigException;
import wtf.indivisible.utils.files.FileUtils;
import wtf.indivisible.utils.json.JsonMapper;

import java.io.File;

/**
 * File based Cfg Storage.
 * <p>
 * Uses pretty printed JSON (via {@link wtf.indivisible.utils.json.Jsonable}) in a single directory.
 */
public class FileCfgStore<T extends Cfg> implements CfgStore {

    private static final Logger LOG = LoggerFactory.getLogger(FileCfgStore.class);

    //// filename patterns
    public static final String SIMPLE_FILENAME_PATTERN = "%s.cfg";
    public static final String VERSIONED_FILENAME_PATTERN = "%s_%s.cfg";

    /** Folder containing Cfgs */
    private final File mCfgFolder;


    public FileCfgStore(final File cfgFolder) {
        if (!FileUtils.canReadFolder(cfgFolder)) {
            throw new ConfigException("Invalid cfgFolder (NULL)");
        } else if (!FileUtils.canReadFolder(cfgFolder)) {
            throw new ConfigException("Invalid cfgFolder (not readable or not a directory)");
        } else {
            mCfgFolder = cfgFolder;
        }
    }

    public FileCfgStore(final String cfgFolderPath) {
        this(new File(cfgFolderPath));
    }


    //// filename construction

    public static String makeFilename(final String cfgName) {
        return String.format(SIMPLE_FILENAME_PATTERN, cfgName);
    }

    public static String makeFilename(final String cfgName, final String cfgVersion) {
        return String.format(VERSIONED_FILENAME_PATTERN, cfgName, cfgVersion);
    }

    public static String makeFilename(final Cfg cfg) {
        return makeFilename(cfg.getName());
    }

    public static String makeFilename(final VersionCfg cfg) {
        return makeFilename(cfg.getName(), cfg.getVersion());
    }

    private File makeFile(final String cfgName, final String cfgVersion) {
        String filename = makeFilename(cfgName, cfgVersion);
        return new File(mCfgFolder, filename);
    }

    private File makeFile(final String cfgName) {
        String filename = makeFilename(cfgName);
        return new File(mCfgFolder, filename);
    }


    //// store implements

    @Override
    public boolean exists(final String cfgName, final String cfgVersion) {
        File cfgFile = makeFile(cfgName, cfgVersion);
        return FileUtils.fileExists(cfgFile);
    }

    @Override
    public boolean exists(final String cfgName) {
        File cfgFile = makeFile(cfgName);
        return FileUtils.fileExists(cfgFile);
    }


    @Override
    public Cfg load(final String cfgName, final String cfgVersion) {
        if (!exists(cfgName, cfgVersion)) {
            LOG.info("Cfg file '{}' doesn't exist.", makeFilename(cfgName, cfgVersion));
            return null;
        } else if (!CfgMapper.hasName(cfgName)) {
            LOG.info("No known class for Cfg {}", cfgName);
            return null;
        } else {
            Class<Cfg> clazz = (Class<Cfg>) CfgMapper.getCfgClass(cfgName);
            File cfgFile = makeFile(cfgName, cfgVersion);
            return JsonMapper.fromJsonFile(cfgFile, clazz);
        }
    }

    @Override
    public T load(final String cfgName) {
        if (!exists(cfgName)) {
            LOG.info("Cfg file '{}' doesn't exist.", makeFilename(cfgName));
            return null;
        } else if (!CfgMapper.hasName(cfgName)) {
            LOG.info("No known class for Cfg {}", cfgName);
            return null;
        } else {
            Class<T> clazz = (Class<T>) CfgMapper.getCfgClass(cfgName);
            File cfgFile = makeFile(cfgName);
            return JsonMapper.fromJsonFile(cfgFile, clazz);
        }
    }

    @Override
    public boolean save(final String cfgName, final String cfgVersion, final Cfg cfg) {
        return false;
    }

    @Override
    public boolean save(final String cfgName, final Cfg cfg) {
        return false;
    }

    @Override
    public boolean save(final Cfg cfg) {
        return false;
    }
}
