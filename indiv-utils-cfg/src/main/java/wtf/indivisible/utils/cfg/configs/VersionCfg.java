package wtf.indivisible.utils.cfg.configs;

import com.fasterxml.jackson.annotation.JsonProperty;
import wtf.indivisible.utils.cfg.VersionedCfg;
import wtf.indivisible.utils.nulls.NullSafe;

/**
 * A {@link SimpleCfg} extension that includes (String) version information
 */
public abstract class VersionCfg extends SimpleCfg implements VersionedCfg {

    @JsonProperty("version")
    private String mVersion;


    //// init
    protected VersionCfg(final String configName, final String version) {
        super(configName);
        mVersion = NullSafe.trim(version, true);
    }


    //// access
    public String getVersion() {
        return mVersion;
    }

    public void setVersion(final String version) {
        mVersion = NullSafe.trim(version, true);
    }


}
