package wtf.indivisible.utils.cfg.util;

import wtf.indivisible.utils.cfg.Cfg;

import java.util.Objects;

/**
 * Basic class for holding {@link wtf.indivisible.utils.cfg.Cfg} descriptors. <br>
 * Holds Name, Version, Class and a String className.
 */
public class CfgDescriptor {

    //// members
    private String mName;
    private String mVersion;
    private Class<? extends Cfg> mClazz;
    private String mClazzName;


    //// init
    public CfgDescriptor(final String name, final String version) {
        mName = name;
        mVersion = version;
    }

    public CfgDescriptor(final String name, final String version, final Class<? extends Cfg> clazz) {
        this(name, version);
        setClazz(clazz);
    }

    public CfgDescriptor(final String name, final String version, final String clazzName) {
        this(name, version);
        mClazz = null;
        mClazzName = clazzName;
    }

    public CfgDescriptor(final String name, final Class<? extends Cfg> clazz) {
        this(name, null, clazz);
    }

    public CfgDescriptor(final String name) {
        this(name, null, (String) null);
    }

    public CfgDescriptor() {
        this(null, null, (String) null);
    }


    //// gets & sets
    public boolean hasName() {
        return CfgDescriptorUtils.isValidCfgName(getName());
    }

    public boolean hasVersion() {
        return CfgDescriptorUtils.isValidCfgVersion(getVersion());
    }

    public boolean hasClazz() {
        return getClazz() != null;
    }


    public String getName() {
        return mName;
    }

    public void setName(final String name) {
        mName = name;
    }

    public String getVersion() {
        return mVersion;
    }

    public void setVersion(final String version) {
        mVersion = version;
    }

    public Class<? extends Cfg> getClazz() {
        return mClazz;
    }

    public void setClazz(final Class<? extends Cfg> clazz) {
        if (clazz != null) {
            setClazzName(clazz.getCanonicalName());
        }
        mClazz = clazz;
    }

    public String getClazzName() {
        return mClazzName;
    }

    public void setClazzName(final String clazzName) {
        mClazzName = clazzName;
    }


    //// overrides
    @Override
    public String toString() {
        return CfgDescriptorUtils.asString(this);
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CfgDescriptor that = (CfgDescriptor) o;
        return getName().equals(that.getName()) &&
                Objects.equals(getVersion(), that.getVersion());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getVersion());
    }
}
