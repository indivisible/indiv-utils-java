package wtf.indivisible.utils.cfg.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import wtf.indivisible.utils.nulls.Nulls;

import java.util.regex.Pattern;

public class CfgDescriptorUtils {

    private static final Logger LOG = LoggerFactory.getLogger(CfgDescriptorUtils.class);


    // Regex rules for names & versions consisting of a-z, A-Z, 0-9, hyphen (-), period (.)
    // length 1+, may not end or start with a hyphen
    private static final char DISALLOWED_START_OR_END = '-';
    private static final String PATTERN_HAS_WHITESPACE = "\\s";
    private static final String PATTERN_ALLOWED_CHARS = "^[-.a-zA-Z0-9]+$";
    private static final Pattern REGEX_HAS_WHITESPACE = Pattern.compile(PATTERN_HAS_WHITESPACE);
    private static final Pattern REGEX_ALLOWED_CHARS = Pattern.compile(PATTERN_ALLOWED_CHARS);

    // compound name & version strings are separated by this char
    public static final char NAME_VERSION_SEP = '_';
    public static final char NAME_CLAZZ_SEP = ':';
    public static final String FILENAME_EXTENSION = ".cfg";

    //// validation

    private static final boolean isValidDescriptor(final String cfgNameOrVersion) {
        if (cfgNameOrVersion == null || cfgNameOrVersion.isEmpty()) {
            LOG.warn("INVALID cfgDescriptor [NULL/empty]");
            return false;
        } else if (REGEX_HAS_WHITESPACE.matcher(cfgNameOrVersion).matches()) {
            LOG.warn("INVALID cfgDescriptor [contains whitespace]");
            return false;
        } else if (!REGEX_ALLOWED_CHARS.matcher(cfgNameOrVersion).matches()) {
            LOG.warn("INVALID cfgDescriptor [contains illegal chars]. Allowed [-.a-zA-Z0-9]");
            return false;
        } else if (DISALLOWED_START_OR_END == cfgNameOrVersion.charAt(0)
                || DISALLOWED_START_OR_END == cfgNameOrVersion.charAt(cfgNameOrVersion.length() - 1)) {
            LOG.warn("INVALID cfgDescriptor [starts/ends with illegal char]. Disallowed [-]");
            return false;
        } else {
            // name ok
            return true;
        }
    }

    public static final boolean isValidCfgName(final String cfgName) {
        return isValidDescriptor(cfgName);
    }

    public static final boolean isValidCfgVersion(final String cfgVersion) {
        return isValidDescriptor(cfgVersion);
    }


    //// string formatting & parsing

    public static String asString(final CfgDescriptor cfgDescriptor) {
        if (cfgDescriptor == null) {
            return null;
        } else if (cfgDescriptor.getName() == null) {
            LOG.warn("CfgDescriptor minimally REQUIRES a 'name'");
            return null;
        }

        StringBuilder sb = new StringBuilder(cfgDescriptor.getName());
        if (cfgDescriptor.hasVersion()) {
            sb.append(CfgDescriptorUtils.NAME_VERSION_SEP)
              .append(cfgDescriptor.getVersion());
        }
        if (cfgDescriptor.getClazzName() != null) {
            sb.append(CfgDescriptorUtils.NAME_CLAZZ_SEP)
              .append(cfgDescriptor.getClazzName());
        }
        return sb.toString();
    }


    public static CfgDescriptor parseFilename(final String cfgFilename) {
        if (Nulls.isNullOrEmpty(cfgFilename)) {
            return null;
        }

        if (cfgFilename.endsWith(CfgDescriptorUtils.FILENAME_EXTENSION)) {
            return parseString(cfgFilename.substring(cfgFilename.length() - CfgDescriptorUtils.FILENAME_EXTENSION.length()));
        } else {
            //NB: assumes NO extension rather than unknown extension
            return parseString(cfgFilename);
        }
    }

    public static CfgDescriptor parseString(final String cfgDescriptorString) {
        if (Nulls.isNullOrEmpty(cfgDescriptorString)) {
            return null;
        }

        // pattern: name_version:class
        String cfgStr = cfgDescriptorString.trim();
        CfgDescriptor cfgDesc = new CfgDescriptor();

        // class name
        int foundClazzSep = cfgStr.lastIndexOf(CfgDescriptorUtils.NAME_CLAZZ_SEP);
        if (foundClazzSep != -1) {
            cfgDesc.setClazzName(cfgStr.substring(foundClazzSep + 1));
            cfgStr = cfgStr.substring(0, foundClazzSep);
        }

        // cfg version
        int foundVersionSep = cfgStr.lastIndexOf(CfgDescriptorUtils.NAME_VERSION_SEP);
        if (foundVersionSep != -1) {
            cfgDesc.setVersion(cfgStr.substring(foundVersionSep + 1));
            cfgStr = cfgStr.substring(0, foundVersionSep);
        }

        // cfg name left
        cfgDesc.setName(cfgStr);
        return cfgDesc;

    }
}
