package wtf.indivisible.utils.tags;

import org.junit.Test;
import wtf.indivisible.utils.id.Ids;

import static org.junit.Assert.*;

public class TagTests {


    private static final int COUNT_CREATE = 1_000;


    // Test helper Tag generation
    private static Tag newRandomTag() {
        return new IndivTag(Ids.UUID.newId(), Ids.MinId.newId());
    }


    @Test
    public void testTagCreate() {
        Tag tag;
        for (int i = 0; i < COUNT_CREATE; i++) {
            tag = new IndivTag(Ids.UUID.newId(), Ids.MinId.newId());
            assertTrue("Newly created IndivTag didn't have an id", tag.hasId());
            assertTrue("Newly created IndivTag didn't have a name", tag.hasName());

            tag = new IndivTag(Ids.MinId.newId(), Ids.UUID.newId());
            assertTrue("Newly created IndivTag didn't have an id", tag.hasId());
            assertTrue("Newly created IndivTag didn't have a name", tag.hasName());
        }
    }


    @Test
    public void testTagEquality() {
        Tag tag1, tag2;
        for (int i = 0; i < COUNT_CREATE; i++) {
            String id = Ids.UUID.newId();
            String name1 = Ids.MinId.newId();
            String name2 = Ids.MinId.newId();

            // name not used for equality checks, only id
            tag1 = new IndivTag(id, name1);
            tag2 = new IndivTag(id, name2);

            assertEquals("Tags with same id failed equality test!!", tag1, tag2);
        }
    }


    @Test
    public void testTaggableCollection() {
        Taggable taggable = new Tagged();

        assertFalse("Newly created Taggable was created non-empty (hasTags())", taggable.hasTags());
        assertTrue("Newly created Taggable was created non-empty (getTags())", taggable.getTags().isEmpty());

        Tag tag1 = newRandomTag();
        Tag tag2 = newRandomTag();
        Tag tag3 = newRandomTag();

        taggable.addTag(tag1);
        taggable.addTag(tag2);

        assertTrue("Taggable with two tags added reports as empty (hasTags())", taggable.hasTags());
        assertTrue("Taggable didn't have recently added Tag", taggable.hasTag(tag1));
        assertTrue("Taggable didn't have recently added Tag", taggable.hasTag(tag1.getId()));
        assertTrue("Taggable didn't have recently added Tag", taggable.hasTag(tag2));
        assertTrue("Taggable didn't have recently added Tag", taggable.hasTag(tag2.getId()));
        assertFalse("Taggable had Tag that was never added", taggable.hasTag(tag3));
        assertFalse("Taggable had Tag that was never added", taggable.hasTag(tag3.getId()));

    }

}
