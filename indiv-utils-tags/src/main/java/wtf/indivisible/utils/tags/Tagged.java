package wtf.indivisible.utils.tags;

import wtf.indivisible.utils.id.Identifiable;

import java.util.ArrayList;
import java.util.List;

public class Tagged implements Taggable {

    private /*volatile*/ List<Tag> tags;


    public Tagged() {
        tags = new ArrayList<>();
    }

    public Tagged(final List<Tag> tags) {
        this.tags = tags;
    }


    @Override
    public int clearTags() {
        int tagCount = hasTags() ? getTags().size()
                                 : 0;
        tags = new ArrayList<>();
        return tagCount;
    }

    @Override
    public List<Tag> getTags() {
        //ASK: mem leak?
        return List.copyOf(tags);
    }

    @Override
    public boolean addTag(final Tag tag) {
        if (!Identifiable.hasId(tag)) {
            // null tag or no id to check if dupe so refuse
            return false;
        } else if (hasTags()) {
            for (Tag t : tags) {
                if (t != null && t.equalsById(tag.getId())) {
                    // duplicate id, refuse
                    return false;
                }
            }
        }
        return tags.add(tag);
        //ASK: sort? On^3?
    }

    @Override
    public boolean removeTag(final Tag tag) {
        return tag != null && removeTag(tag.getId());
    }

    @Override
    public boolean removeTag(final String tagId) {
        if (hasTags()) {
            for (int i = 0; i < tags.size(); i++) {
                if (tags.get(i).equalsById(tagId)) {
                    return tags.remove(i) != null;
                }
            }
        }
        return false;
    }

}
