package wtf.indivisible.utils.tags;

import wtf.indivisible.utils.nulls.Nulls;

import java.util.Objects;

public class IndivTag implements Tag {

    private final String mId;
    private final String mName;


    public IndivTag(final String id, final String name) throws IllegalArgumentException {
        String cleanId = (id == null) ? null : id.trim();
        String cleanName = (name == null) ? null : name.trim();

        if (Nulls.isNullOrEmpty(id)) {
            throw new IllegalArgumentException("Supplied blank/null id for new Tag");
        } else if (Nulls.isNullOrEmpty(name)) {
            throw new IllegalArgumentException("Supplied blank/null name for new Tag");
        } else {
            mId = cleanId;
            mName = cleanName;
        }
    }

    @Override
    public String getId() {
        return mId;
    }

    @Override
    public String getName() {
        return mName;
    }


    ////////////////////////////////////////////////////////////////////////////
    ////  Object overrides
    ////////////////////////////////////////////////////////////////////////////

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (!(o instanceof IndivTag)) return false;
        final IndivTag indivTag = (IndivTag) o;
        return getId().equals(indivTag.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }

    @Override
    public String toString() {
        return "IndivTag{" + mId + ':' + mName + '}';
    }
}
