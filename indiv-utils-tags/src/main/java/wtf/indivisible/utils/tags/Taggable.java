package wtf.indivisible.utils.tags;

import wtf.indivisible.utils.id.Identifiable;

import java.util.List;

public interface Taggable {

    List<Tag> getTags();

    int clearTags();

    boolean addTag(final Tag tag);

    boolean removeTag(final Tag tag);

    boolean removeTag(final String tagId);


    ////////////////////////////////////////////////////////////////////////////
    ////  default methods for simple checks, no modification
    ////////////////////////////////////////////////////////////////////////////

    /**
     * Check whether there are any Tags
     *
     * @return
     */
    // assumes null values disallowed
    default boolean hasTags() {
        return getTags() != null && !getTags().isEmpty();
    }

    /**
     * Checks for the existence of a Tag with the supplied instance's id.
     *
     * @param tag
     * @return
     */
    default boolean hasTag(final Tag tag) {
        if (Identifiable.hasId(tag)) {
            return hasTag(tag.getId());
        } else {
            return false;
        }
    }

    /**
     * Checks for the existence of a Tag with the supplied id.
     *
     * @param tagId
     * @return
     */
    default boolean hasTag(final String tagId) {
        if (hasTags()) {
            for (Tag tag : getTags()) {
                assert tag != null;
                if (tag.equalsById(tagId)) {
                    return true;
                }
            }
        }
        return false;
    }


}
