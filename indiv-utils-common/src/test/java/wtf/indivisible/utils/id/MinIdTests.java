package wtf.indivisible.utils.id;

import org.junit.Test;

import static org.junit.Assert.*;

public class MinIdTests {

    private static MinIdHelper MINID_HELPER = new MinIdHelper();

    private static final int COUNT_GENERATIONS = 10_000;
    private static final int[] LENGTHS_TO_GEN = {4, 8, 12, 16, 20, 24, 32, 64, 124, MinIdHelper.MAX_LENGTH};
    private static final int[] LENGTHS_TO_FAIL = {-1, 0, 1, 5, 11, 13,
        MinIdHelper.MAX_LENGTH + 1, MinIdHelper.MAX_LENGTH + 4};

    private static final String[] GOOD_IDS = {
        "rjpM",
        "SmCyZ+hJ",
        "Lvcyr+u8M96f",
        "UqDYauH5Px+05O5q",
        "Fv4qD2tLvAs3rJa16VUQ",
        "D6Xjn+PC6ufuQuGF14n0XO+Y",
        "ukweBwL+aYfNsBkTInRCYfJKg09T9rrR",
        "75gb2GKcPEaFMI7YhKG6hEmuhjQ4Q9CgmVEHVyUwsm4naFJYw2rlQ/ECB6ETKtlZ",
        "o7cE",
        "t/LmaKe8",
        "Hu3TXn5LhWuo",
        "GDQbDu3xSFAzvcAO",
        "iN74Hh6hSr8e+4oXSfuo",
        "bk52/YSiOfKxkpTZ6IQhBJhK",
        "tnV2MaZ5c8ivSjtMs4KTX7h1mcvj7QhW",
        "xEgNf1nfodhZFaH5W+rt6ywutCvaOODNp8jWuW3FmJdbpdSrPjZNWqWRlFeJ0dMj",
        "DhKh",
        "ipgK5Dsk",
        "GeRqp1nYHOJC",
        "bdWbFp3m7477gGTb",
        "LysuZs/JZNwY1tTi+U5j",
        "YgDCvDgXCbWq1Bv2aRrUQnjQ",
        "tc2bO34xxJqOhPRXHiwDjcEbKArliiVq",
        "JdkuV9fCwbayAayRw27Y/v4wcFf8E7sldUhGPikd3i3NqSV4BKygmn2vwP9DUa9D",
        "Qcc0",
        "BXJMTviF",
        "dDT0EVZR8DV7",
        "QyNHAr/fJaLnQ0zK",
        "R4wumqDyFrNoD3y6hqOo",
        "+SCENiYGeT/a7I8Dj0u0R1Ex",
        "+GTuB8sSR6B6CWZ2AsUyVbmI56mdLyjt",
        "cZkbbbrEo/4/vtc+B+psAiJY069eKfJOhl1RCPTrOoPVvW41mlv5veYMmWCrN6hm",
    };

    // bad because: length, empty, padding (all ids must not contain whitespace)
    private static final String[] BAD_IDS = {
        null, "", "  ", "\t",
        "Qcc0a",
        "BXJMTvi",
        "dDT0EVZR8DV7 ",
        "QyNHAr/fJaLnQ0z ",
        " 4wumqDyFrNoD3y6hqOo",
        "+SCENiYGe /a7I8Dj0u0R1Ex",
        "+GTuB8sSR6B6CWZ2AsUyVbmI56mdLyjt",
        "cZkbb-rEo/4/vtc+B+psAiJY069eKfJOhl1RCPTrOoPVvW41mlv5veYMmWCrN6hm",
    };


    ////////////////////////////////////////////////////////////////////////////
    ////  tests
    ////////////////////////////////////////////////////////////////////////////

    @Test
    public void testMinIdGeneration() {
        // only testing MinIdHelper.newId(int) since its used by MinIdHelper.newInt() anyway

        int genLengthsMaxIndex = LENGTHS_TO_GEN.length - 1;
        int desiredIdLength;
        String id;
        for (int i = 0; i < COUNT_GENERATIONS; i++) {
            desiredIdLength = LENGTHS_TO_GEN[i % genLengthsMaxIndex];
            id = MINID_HELPER.newId(desiredIdLength);

            assertNotNull("INVALID id generated (NULL)", id);
            assertTrue("INVALID id generated (EMPTY)", id.length() > 0);
            assertFalse("INVALID id generated (NONE)", MINID_HELPER.isNoneId(id));
            assertEquals("INVALID id generated (Bad length)", id.length(), desiredIdLength);
            //assertTrue("INVALID id generated (!isValid)", MINID_HELPER.isValidId(id));
        }
    }

    @Test
    public void testMiniIdBadLengthGeneration() {
        for (int i = 0; i < LENGTHS_TO_FAIL.length - 1; i++) {
            try {
                MINID_HELPER.newId(LENGTHS_TO_FAIL[i]);
            } catch (final IllegalArgumentException e) {
                // pass
                continue;
            }
            throw new AssertionError("newId(int) generated a bad length MinId: " + LENGTHS_TO_FAIL[i]);
        }
    }


    @Test
    public void testMinIdEquality() {
        String id1, id2;
        for (int i = 0; i < COUNT_GENERATIONS / 2; i++) {
            id1 = MINID_HELPER.newId();
            id2 = new String(id1.toCharArray());    // deep copy to ensure chars equated

            assertTrue("ERROR id clone didn't equal itself: " + id1 + "/" + id2,
                IdUtils.equalIds(id1, id2));
            assertTrue("ERROR id clone didn't equal itself: " + id1 + "/" + id2,
                IdUtils.equalIds(id1, id2, true, false));
        }
    }

    @Test
    public void testMinIdNone() {
        //NB: already tested that generation won't produce "none"s
        assertTrue("FAILED to validate UUID none value as none: '" + MinIdHelper.NONE_ID + "'",
            MINID_HELPER.isNoneId(MinIdHelper.NONE_ID));
    }

    @Test
    public void testMiniIdValidity() {
        for (String goodId : GOOD_IDS) {
            assertTrue("GOOD_ID[" + goodId + "] failed isValidId() check",
                MINID_HELPER.isValidId(goodId));
        }
    }


}
