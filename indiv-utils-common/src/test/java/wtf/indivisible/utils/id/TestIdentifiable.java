package wtf.indivisible.utils.id;

public class TestIdentifiable implements Identifiable {

    private final String stringId;

    public TestIdentifiable(final String id) {
        stringId = id;
    }

    @Override
    public String getId() {
        return stringId;
    }
}
