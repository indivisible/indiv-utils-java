package wtf.indivisible.utils.id;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Locale;
import java.util.regex.Pattern;

import static org.junit.Assert.assertTrue;

public class UUIDTests {

    private static final Logger LOG = LoggerFactory.getLogger(UUIDTests.class);
    private static final UUIDHelper UUID_HELPER = new UUIDHelper();
    private static final int GENERATE_ID_COUNT = 10_000;

    /**
     * Under normal conditions, case doesn't matter but for testing we want to
     * check lower vs upper variants so this is a lowercase only regex.
     */
    private static final String LOWERCASE_REGEX = "^" +
                                                  "[a-f0-9]{8}-" +
                                                  "[a-f0-9]{4}-" +
                                                  "[a-f0-9]{4}-" +
                                                  "[a-f0-9]{4}-" +
                                                  "[a-f0-9]{12}$";
    private static final Pattern PATTERN_LOWERCASE_ID = Pattern.compile(LOWERCASE_REGEX);


    @Test
    public void testIds() {
        LOG.info("Testing/Validating [{}] new Ids...", GENERATE_ID_COUNT);

        String strId;
        for (int i = 0; i < GENERATE_ID_COUNT; i++) {
            strId = UUID_HELPER.newId();

            assertTrue("Freshly generated Id failed isValid() test: " + strId, UUID_HELPER.isValidId(strId));
            assertTrue("Freshly generated Id was NOT lowercase hex: " + strId, PATTERN_LOWERCASE_ID.matcher(strId).matches());
            assertTrue("Freshly generated Id didn't equal itself: " + strId, IdUtils.equalIds(strId, strId));
            assertTrue("Freshly generated Id (altered case) didn't equal itself: " + strId,
                IdUtils.equalIds(strId.toUpperCase(Locale.ENGLISH), strId, true, true));

            Identifiable id1 = new TestIdentifiable(strId);
            Identifiable id2 = new TestIdentifiable(strId);
            Identifiable id3 = new TestIdentifiable(strId.toUpperCase(Locale.ENGLISH));
            assertTrue("Wrapped Id didn't have an id", id1.hasId());
            assertTrue("Wrapped Id didn't contain orig id: " + strId, id1.equalsById(strId));
            assertTrue("Wrapped Id didn't equal copy of itself: " + strId, id1.equalsById(id2));
            assertTrue("Wrapped Id didn't equal copy of itself (init as upper): " + strId,
                IdUtils.equalIds(id1.getId(), id3.getId(), true, true));
        }
    }


    @Test
    public void testUUIDNone() {
        //NB: already tested that generation won't produce "none"s
        assertTrue("FAILED to validate UUID none value as none: '" + UUIDHelper.NONE_ID + "'",
            UUID_HELPER.isNoneId(UUIDHelper.NONE_ID));
    }

}
