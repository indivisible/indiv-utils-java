package wtf.indivisible.utils.generics;

import org.junit.Test;
import wtf.indivisible.utils.generics.stringables.SimpleStringable;
import wtf.indivisible.utils.nulls.Nulls;

import java.util.Random;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Tests of the {@link Stringable} interface implementations.
 * <p>
 * Note that most non-trivial cases make use of Jsonable (available via indiv-utils-json)
 * rather than manually handling/formatting complex data types or annoying edge-cases.
 */
public class StringableTests {

    private static final Random RAND = new Random();

    private static final SimpleStringable[] TEST_SIMPLE_STRINGABLES = {
            new SimpleStringable(null, null, null),
            new SimpleStringable("test value", null, null),
            new SimpleStringable("test_stringables", -123, null),
            new SimpleStringable("MIXEDcase", +456, 123.456D),
            new SimpleStringable("randomsA", RAND.nextInt(), RAND.nextDouble()),
            new SimpleStringable("randomsB", RAND.nextInt(), RAND.nextDouble()),
            new SimpleStringable("randomsC", RAND.nextInt(), RAND.nextDouble()),
            new SimpleStringable("randomsD", RAND.nextInt(), RAND.nextDouble())
    };


    @Test
    public void testStringables() {

        // empty instance to call fromString() on
        SimpleStringable ss = new SimpleStringable();

        String asString;
        SimpleStringable fromString;
        for (SimpleStringable st : TEST_SIMPLE_STRINGABLES) {
            assertNotNull("Encountered NULL SimpleStringable in test data", st);

            asString = st.asString();
            assertTrue("Produced NULL asString from SimpleStringable", Nulls.notNull(asString));
            fromString = ss.fromString(asString);
            assertNotNull("Produced NULL instance of SimpleStringable fromString", fromString);
            assertEquals("SimpleStringable did not match itself after deserialization: " + st.getString(),
                    st, fromString);
            assertEquals("SimpleStringable hashCodes did not match after deserialization: " + st.getString(),
                    st.hashCode(), fromString.hashCode());
        }
    }

}
