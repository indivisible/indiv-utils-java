package wtf.indivisible.utils.generics.stringables;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import wtf.indivisible.ex.BadArgumentException;
import wtf.indivisible.utils.generics.Stringable;

import java.util.Objects;

/**
 * Manual Stringable with basic members.
 * <p>
 * Is limited by not allowing SEP values in the internal String due to the way values are serialized.
 */
public class SimpleStringable implements Stringable<SimpleStringable> {

    private static final Logger LOG = LoggerFactory.getLogger(SimpleStringable.class);

    //// formatting
    private static final char FIELD_TYPE_SEP = ':';
    private static final char FIELD_VALUE_SEP = ',';
    private static final char FIELD_STRING = 's';
    private static final char FIELD_INTEGER = 'i';
    private static final char FIELD_DOUBLE = 'd';

    //// members
    private String mString;
    private Integer mInteger;
    private Double mDouble;

    //// init
    public SimpleStringable(final String aString, final Integer anInteger, final Double aDouble) {
        mString = aString;
        mInteger = anInteger;
        mDouble = aDouble;
    }

    public SimpleStringable() {
        this(null, null, null);
    }

    //// gets & sets
    public String getString() {
        return mString;
    }

    public void setString(final String string) {
        mString = string;
    }

    public Integer getInteger() {
        return mInteger;
    }

    public void setInteger(final Integer integer) {
        mInteger = integer;
    }

    public Double getDouble() {
        return mDouble;
    }

    public void setDouble(final Double aDouble) {
        mDouble = aDouble;
    }

    //// Stringable implements
    @Override
    public String asString() {
        StringBuilder sb = new StringBuilder();
        if (mString != null) {
            sb.append(FIELD_STRING).append(FIELD_TYPE_SEP)
              .append(mString).append(FIELD_VALUE_SEP);
        }
        if (mInteger != null) {
            sb.append(FIELD_INTEGER).append(FIELD_TYPE_SEP)
              .append(mInteger).append(FIELD_VALUE_SEP);
        }
        if (mDouble != null) {
            sb.append(FIELD_DOUBLE).append(FIELD_TYPE_SEP)
              .append(mDouble).append(FIELD_VALUE_SEP);
        }

        if (sb.length() != 0) {
            // all nulls if length 0
            sb.deleteCharAt(sb.length() - 1);
        }
        return sb.toString();
    }

    @Override
    public SimpleStringable fromString(final String s) {
        if (s == null) return null;
        else if (s.isEmpty()) {
            // special case with all null members
            return new SimpleStringable();
        }

        SimpleStringable ss = new SimpleStringable();
        String[] parts = s.split(FIELD_VALUE_SEP + "");
        char typeChar;
        String value;
        for (String part : parts) {
            typeChar = part.charAt(0);
            value = part.substring(2);

            try {
                switch (typeChar) {
                    case FIELD_STRING:
                        ss.setString(value);
                        break;
                    case FIELD_INTEGER:
                        ss.setInteger(Integer.parseInt(value));
                        break;
                    case FIELD_DOUBLE:
                        ss.setDouble(Double.parseDouble(value));
                        break;
                    default:
                        throw new BadArgumentException("Unknown typeChar: " + typeChar);
                }
            } catch (final NumberFormatException e) {
                LOG.error("Caught a NumberFormatException parsing value '{}'", value);
                LOG.error("Exception: ", e);
            }
        }

        return ss;
    }


    //// equals & hashCode


    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (!(o instanceof SimpleStringable)) return false;
        SimpleStringable that = (SimpleStringable) o;
        return Objects.equals(getString(), that.getString()) &&
                Objects.equals(getInteger(), that.getInteger()) &&
                Objects.equals(getDouble(), that.getDouble());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getString(), getInteger(), getDouble());
    }
}
