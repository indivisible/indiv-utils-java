package wtf.indivisible.utils.dates;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

public class DateTestCases {

    public static final Long epochMillis = System.currentTimeMillis();
    public static final Date date = new Date();
    public static final Instant instant = Instant.now();
    public static final LocalDate localDate = LocalDate.now();
    public static final LocalDateTime localDateTime = LocalDateTime.now();

}
