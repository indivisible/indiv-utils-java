package wtf.indivisible.utils.dates;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import wtf.indivisible.utils.nulls.Nulls;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class DateFormatTests {

    private static final Logger LOG = LoggerFactory.getLogger(DateFormatTests.class);

    private static final boolean PRINT_FORMATS = false;

    @Test
    public void testDateFormats() {

        String epochFormat;
        String dateFormat;
        String instantFormat;
        String localDateFormat;
        String localDateTimeFormat;

        for (DateFormats df : DateFormats.values()) {

            if (PRINT_FORMATS) LOG.info("TESTING FORMAT: " + df.name());

            epochFormat = df.format(DateTestCases.epochMillis);
            dateFormat = df.format(DateTestCases.date);
            instantFormat = df.format(DateTestCases.instant);
            localDateFormat = df.format(DateTestCases.localDate);
            localDateTimeFormat = df.format(DateTestCases.localDateTime);

            assertTrue("epochFormat FAILED", Nulls.notNullOrEmpty(epochFormat));
            assertTrue("dateFormat FAILED", Nulls.notNullOrEmpty(dateFormat));
            assertTrue("instantFormat FAILED", Nulls.notNullOrEmpty(instantFormat));
            assertTrue("localDateFormat FAILED", Nulls.notNullOrEmpty(localDateFormat));
            assertTrue("localDateTimeFormat FAILED", Nulls.notNullOrEmpty(localDateTimeFormat));

            if (PRINT_FORMATS) {
                LOG.info("  Long:          " + epochFormat);
                LOG.info("  Date:          " + dateFormat);
                LOG.info("  Instant:       " + instantFormat);
                LOG.info("  LocalDate:     " + localDateFormat);
                LOG.info("  LocalDateTime: " + localDateTimeFormat);
                LOG.info("----------");
            }

        }

    }

}
