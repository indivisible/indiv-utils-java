package wtf.indivisible.utils.dates;

import org.junit.Test;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

import static org.junit.Assert.assertNotNull;


public class DateConvertTests {


    @Test
    public void testToEpochMillis() {

        Long[] epochs = {
                DateConverter.toEpochMillis(DateTestCases.date),
                DateConverter.toEpochMillis(DateTestCases.instant),
                DateConverter.toEpochMillis(DateTestCases.localDate),
                DateConverter.toEpochMillis(DateTestCases.localDateTime),
        };

        for (int i=0; i<epochs.length; i++) {
            assertNotNull("Encountered NULL conversion to Long (epochMillis) at position " + i, epochs[i]);
        }
    }

    @Test
    public void testToDate() {
        Date[] dates = {
                DateConverter.toDate(DateTestCases.epochMillis),
                DateConverter.toDate(DateTestCases.instant),
                DateConverter.toDate(DateTestCases.localDate),
                DateConverter.toDate(DateTestCases.localDateTime),
        };

        for (int i=0; i<dates.length; i++) {
            assertNotNull("Encountered NULL conversion to java.util.Date at position " + i, dates[i]);
        }
    }

    @Test
    public void testToInstant() {
        Instant[] instants = {
                DateConverter.toInstant(DateTestCases.epochMillis),
                DateConverter.toInstant(DateTestCases.date),
                DateConverter.toInstant(DateTestCases.localDate),
                DateConverter.toInstant(DateTestCases.localDateTime),
        };

        for (int i=0; i<instants.length; i++) {
            assertNotNull("Encountered NULL conversion to java.time.Instant at position " + i, instants[i]);
        }
    }

    @Test
    public void testToLocalDate() {
        LocalDate[] localDates = {
                DateConverter.toLocalDate(DateTestCases.epochMillis),
                DateConverter.toLocalDate(DateTestCases.date),
                DateConverter.toLocalDate(DateTestCases.instant),
                DateConverter.toLocalDate(DateTestCases.localDateTime),
        };

        for (int i=0; i<localDates.length; i++) {
            assertNotNull("Encountered NULL conversion to java.time.LocalDate at position " + i, localDates[i]);
        }
    }

    @Test
    public void testToLocalDateTime() {
        LocalDateTime[] localDateTimes = {
                DateConverter.toLocalDateTime(DateTestCases.epochMillis),
                DateConverter.toLocalDateTime(DateTestCases.date),
                DateConverter.toLocalDateTime(DateTestCases.instant),
                DateConverter.toLocalDateTime(DateTestCases.localDate),
        };

        for (int i=0; i<localDateTimes.length; i++) {
            assertNotNull("Encountered NULL conversion to java.time.LocalDateTime at position " + i, localDateTimes[i]);
        }
    }
}
