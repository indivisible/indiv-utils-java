package wtf.indivisible.ex;

/**
 * Base custom Exception type for Checked/Compile-time Exceptions
 */
public abstract class IndivCheckedException extends Exception {

    public IndivCheckedException(final String message) {
        super(message);
    }

    public IndivCheckedException(final String message, final Throwable cause) {
        super(message, cause);
    }

}
