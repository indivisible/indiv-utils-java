package wtf.indivisible.ex;

/**
 * Runtime Exception triggered by bad/invalid arguments passed/supplied to a method.
 */
public class BadArgumentException extends IndivRuntimeException {

    public BadArgumentException(final String message) {
        super(message);
    }

    public BadArgumentException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
