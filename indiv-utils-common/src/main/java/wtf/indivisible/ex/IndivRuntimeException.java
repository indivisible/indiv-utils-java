package wtf.indivisible.ex;

/**
 * Base custom Exception type for Uncheck/Runtime Exceptions
 */
public abstract class IndivRuntimeException extends RuntimeException {

    public IndivRuntimeException(final String message) {
        super(message);
    }

    public IndivRuntimeException(final String message, final Throwable cause) {
        super(message, cause);
    }

}
