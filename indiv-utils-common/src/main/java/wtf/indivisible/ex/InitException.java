package wtf.indivisible.ex;

/**
 * Checked Exception thrown when initialisation fails or is impossible to attempt.
 */
public class InitException extends IndivCheckedException {

    public InitException(final String message) {
        super(message);
    }

    public InitException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
