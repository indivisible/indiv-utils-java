package wtf.indivisible.ex;

/**
 * Thrown from methods/constructors that have not yet been implemented.
 * Indicates a WIP or todo state.
 */
public class UnimplementedException extends IndivRuntimeException {

    public UnimplementedException(final String message) {
        super(message);
    }

    public UnimplementedException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
