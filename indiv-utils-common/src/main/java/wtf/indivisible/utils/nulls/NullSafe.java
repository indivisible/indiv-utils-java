package wtf.indivisible.utils.nulls;

import java.util.Collection;
import java.util.Map;

/**
 * Null safe wrappers for common tests/checks on common types.
 */
public class NullSafe {

    //// defaults
    public static final boolean DEFAULT_NULLS_CONSIDERED_EQUAL = true;
    public static final boolean DEFAULT_EMPTY_TRIM_RETURNS_NULL = false;
    public static final int DEFAULT_NULL_LENGTH_RETURN = 0;


    //// equals

    public static boolean equals(final Object o1, final Object o2) {
        return equals(o1, o2, DEFAULT_NULLS_CONSIDERED_EQUAL);
    }

    public static boolean equals(final Object o1, final Object o2, final boolean nullsConsideredEqual) {
        if (o1 == null && o2 == null) {
            return nullsConsideredEqual;
        } else {
            return (o1 != null && o1.equals(o2));
        }
    }


    //// trim

    public static String trim(final String str) {
        return trim(str, DEFAULT_EMPTY_TRIM_RETURNS_NULL);
    }

    public static String trim(final String str, final boolean emptyReturnsNull) {
        if (str == null) return null;

        String s = str.trim();
        if (emptyReturnsNull && s.isEmpty()) {
            return null;
        } else {
            return s;
        }
    }


    //// length & size

    public static int length(final String s) {
        return length(s, DEFAULT_NULL_LENGTH_RETURN);
    }

    public static int length(final String s, final int nullReturnValue) {
        return (s == null) ? nullReturnValue : s.length();
    }

    public static <T> int length(final T[] array) {
        return length(array, DEFAULT_NULL_LENGTH_RETURN);
    }

    public static <T> int length(final T[] array, final int nullReturnValue) {
        return (array == null) ? nullReturnValue : array.length;
    }

    public static <T> int size(final Collection<T> collection) {
        return size(collection, DEFAULT_NULL_LENGTH_RETURN);
    }

    public static <T> int size(final Collection<T> collection, final int nullReturnValue) {
        return (collection == null) ? nullReturnValue : collection.size();
    }

    public static <K, V> int size(final Map<K, V> map) {
        return size(map, DEFAULT_NULL_LENGTH_RETURN);
    }

    public static <K, V> int size(final Map<K, V> map, final int nullReturnValue) {
        return (map == null) ? nullReturnValue : map.size();
    }

}
