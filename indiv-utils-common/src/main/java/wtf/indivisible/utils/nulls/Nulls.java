package wtf.indivisible.utils.nulls;

import java.util.Collection;
import java.util.Map;

/**
 * Convenience methods for handling possible nulls with common types.
 */
public class Nulls {

    //========================================================//
    //  generic

    public static boolean isNull(final Object val) {
        return val == null;
    }

    public static boolean notNull(final Object val) {
        return val != null;
    }

    public static <T> T ensureNotNull(final T val, final T fallbackValue) {
        if (val == null) {
            return fallbackValue;
        } else {
            return val;
        }
    }

    public static void assertNotNull(final Object val, final Throwable throwOnNull) throws Throwable {
        if (val == null) {
            if (throwOnNull != null) throw throwOnNull;
            else throw new AssertionError("Supplied value NULL");
        }
    }

    public static void assertNotNull(final Object val) throws Throwable {
        assertNotNull(val, null);
    }


    //========================================================//
    //  strings

    public static boolean isNullOrEmpty(final String str) {
        return str == null || str.isEmpty();
    }

    public static boolean notNullOrEmpty(final String str) {
        return str != null && !str.isEmpty();
    }

    public static boolean isNullOrEmptyWithTrim(final String str) {
        return str == null || isNullOrEmpty(str.trim());
    }

    public static boolean notNullOrEmptyWithTrim(final String str) {
        return str != null && notNullOrEmpty(str.trim());
    }

    public static String ensureNotNullOrEmpty(final String str, final String fallbackValue) {
        if (Nulls.isNullOrEmpty(str)) {
            return fallbackValue;
        } else {
            return str;
        }
    }

    public static String ensureNotNullOrEmptyWithTrim(final String str, final String fallbackValue) {
        String s = NullSafe.trim(str);
        return ensureNotNullOrEmpty(s, fallbackValue);
    }

    public static void assertNotNullOrEmpty(final String str, final Throwable throwOnError) throws Throwable {
        if (Nulls.isNullOrEmpty(str)) {
            if (throwOnError != null) throw throwOnError;
            else throw new AssertionError("Supplied String NULL or EMPTY");
        }
    }

    public static void assertNotNullOrEmpty(final String str) throws Throwable {
        assertNotNullOrEmpty(str, null);
    }

    public static void assertNotNullOrEmptyWithTrim(final String str, final Throwable throwOnError) throws Throwable {
        if (Nulls.isNullOrEmptyWithTrim(str)) {
            if (throwOnError != null) throw throwOnError;
            else throw new AssertionError("Supplied String NULL or EMPTY (with trim)");
        }
    }

    public static void assertNotNullOrEmptyWithTrim(final String str) throws Throwable {
        assertNotNullOrEmptyWithTrim(str, null);
    }


    //========================================================//
    //  collections

    public static <T> boolean isNullOrEmpty(final T[] array) {
        return array == null || array.length == 0;
    }

    public static <T> boolean notNullOrEmpty(final T[] array) {
        return array != null && array.length > 0;
    }

    public static <T> void assertNotNullOrEmpty(final T[] array) throws Throwable {
        assertNotNullOrEmpty(array, null);
    }

    public static <T> void assertNotNullOrEmpty(final T[] array, final Throwable throwOnError) throws Throwable {
        if (isNullOrEmpty(array)) {
            if (throwOnError != null) throw throwOnError;
            else throw new AssertionError("Supplied array NULL or EMPTY");
        }
    }

    public static <T> boolean isNullOrEmpty(final Collection<T> collection) {
        return collection == null || collection.isEmpty();
    }

    public static <T> boolean notNullOrEmpty(final Collection<T> collection) {
        return collection != null && !collection.isEmpty();
    }

    public static <T> void assertNotNullOrEmpty(final Collection<T> collection) throws Throwable {
        assertNotNullOrEmpty(collection, null);
    }

    public static <T> void assertNotNullOrEmpty(final Collection<T> collection, final Throwable throwOnError) throws Throwable {
        if (isNullOrEmpty(collection)) {
            if (throwOnError != null) throw throwOnError;
            else throw new AssertionError("Supplied collection NULL or EMPTY");
        }
    }

    public static <K, V> boolean isNullOrEmpty(final Map<K, V> map) {
        return map != null && !map.isEmpty();
    }

    public static <K, V> boolean notNullOrEmpty(final Map<K, V> map) {
        return map != null && !map.isEmpty();
    }

    public static <K, V> void assertNotNullOrEmpty(final Map<K, V> map) throws Throwable {
        assertNotNullOrEmpty(map, null);
    }

    public static <K, V> void assertNotNullOrEmpty(final Map<K, V> map, final Throwable throwOnError) throws Throwable {
        if (isNullOrEmpty(map)) {
            if (throwOnError != null) throw throwOnError;
            else throw new AssertionError("Supplied map NULL or EMPTY");
        }
    }


}
