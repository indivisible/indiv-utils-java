package wtf.indivisible.utils.generics;

/**
 * Interface for any class/model that allows for versioning
 * <p>
 * Generic to be defined for the raw version type.
 */
public interface Versioned<T> {

    T getVersion();

    default boolean hasVersion() {
        return getVersion() != null;
    }

}
