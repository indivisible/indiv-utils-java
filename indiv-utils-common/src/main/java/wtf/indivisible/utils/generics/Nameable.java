package wtf.indivisible.utils.generics;

import wtf.indivisible.utils.nulls.Nulls;

public interface Nameable {

    String getName();

    default boolean hasName() {
        return Nulls.notNullOrEmpty(getName());
    }

    static boolean hasName(final Nameable n) {
        return n != null && n.hasName();
    }

}
