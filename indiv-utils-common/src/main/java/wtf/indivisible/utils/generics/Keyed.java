package wtf.indivisible.utils.generics;

import wtf.indivisible.utils.nulls.Nulls;

public interface Keyed {

    String getKey();

    default boolean hasKey() {
        return Nulls.notNullOrEmpty(getKey());
    }

    static boolean hasKey(final Keyed k) {
        return k != null && k.hasKey();
    }

}
