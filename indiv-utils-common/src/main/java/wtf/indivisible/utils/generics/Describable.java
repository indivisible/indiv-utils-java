package wtf.indivisible.utils.generics;

/**
 * Interface for classes that have a title (required) and description (optional) textual fields.
 */
public interface Describable {

    /**
     * Get the object's title or name.
     * <p>
     * Note that this method should never return 'null' or 'empty string'
     * as it may be required for basic functionality.
     *
     * @return String title/name.
     */
    String getTitle();

    /**
     * Get the object's description or summary.
     * Note this is NOT a text body field and
     * should only be used for short form summaries.
     *
     * @return String description/summary (or null).
     */
    String getDescription();
    
}
