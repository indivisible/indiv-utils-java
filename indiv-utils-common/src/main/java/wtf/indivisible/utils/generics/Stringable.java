package wtf.indivisible.utils.generics;

import java.io.Serializable;

/**
 * Interface for classes that can be (de)serialised through String representations.
 * <p>
 * Implementations MUST be written so that the output of {@link Stringable#asString()}
 * can be used as an input to {@link Stringable#fromString(String)} to reconstruct the
 * given instance.
 * <p>
 * Not all member or class internals necessarily need to be recreated/reconstructed
 * but any public facing members SHOULD be included and all members involved in an
 * {@link Object#equals(Object)} or {@link Object#hashCode()} MUST be included.
 */
public interface Stringable<T extends Stringable<T>> extends Serializable {

    /**
     * Separate from {@link Object#toString()} in that it is required
     * for the output of this method to allow reconstruction via
     * the {@link Stringable#fromString(String)} method.
     *
     * @return String representation of the Stringable object
     */
    String asString();

    /**
     * Construct or modify an instance of the class
     * from the supplied String representation.
     * <p>
     * Format, encoding, validation and state of the original instance is implementation dependent.
     *
     * @param s serialised String
     * @return instance of the Stringable
     */
    //ASK: Would really rather this be a static method but limited by java's interfaces
    T fromString(final String s);

}
