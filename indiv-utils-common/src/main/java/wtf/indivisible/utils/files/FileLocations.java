package wtf.indivisible.utils.files;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import wtf.indivisible.utils.nulls.Nulls;
import wtf.indivisible.utils.props.JvmProps;
import wtf.indivisible.utils.props.SysProps;

import java.io.File;

/**
 * Class to expose common use File locations.
 */
//TODO: check what is and isn't OS agnostic
public class FileLocations {

    private static final Logger LOG = LoggerFactory.getLogger(FileLocations.class);


    //-----------------------------------------------------------------------//
    //  App context locations

    public static File getAppLocation() {
        //TODO
        return null;
    }

    public static File getCurrentDir() {
        //TODO
        return null;
    }

    public static File getAppHome(final String appName) {
        //TODO: select a common place for all indiv-apps data
        return null;
    }


    //-----------------------------------------------------------------------//
    //  User locations

    public static File getUserHome() {

        final String userPath = SysProps.getString(JvmProps.KEY_USER_HOME);
        if (Nulls.isNullOrEmptyWithTrim(userPath)) {
            LOG.warn("Unable to discover the user's home directory.");
            return null;
        }

        File userHome = new File(userPath);
        if (!FileUtils.folderExists(userHome)) {
            LOG.warn("User home could not be found [{}]", FileUtils.path(userHome));
            return null;
        } else if (!FileUtils.canReadFolder(userHome)) {
            LOG.warn("User home exists but can't be read. [{}]", FileUtils.path(userHome));
        } else if (!FileUtils.canWriteFolder(userHome)) {
            LOG.warn("User home exists but can't be written. [{}]", FileUtils.path(userHome));
        }

        // fallthrough return once exists, only logging possible issues on read/write
        return userHome;
    }

}
