package wtf.indivisible.utils.files;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import wtf.indivisible.utils.nulls.Nulls;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Common file I/O static methods
 */
public class FileUtils {

    private static final Logger LOG = LoggerFactory.getLogger(FileUtils.class);

    //TODO: iterate on shutdown hook
    // key = path (trailing / for dir), value = doDelete (and recurse)
    private static Map<String, File> sTempDirCleanupMap = new HashMap<>();


    ////////////////////////////////////////////////////////////
    ////    FILES
    ////////////////////////////////////////////////////////////

    //========================================================//
    //  file test
    //========================================================//

    public static boolean fileExists(final File file) {
        return file != null
                && file.exists()
                && file.isFile();
    }

    public static boolean canReadFile(final File file) {
        return fileExists(file) && file.canRead();
    }

    public static boolean canWriteFile(final File file) {
        return fileExists(file) && file.canWrite();
    }


    //========================================================//
    //  file read
    //========================================================//

    public static byte[] readFileBytes(final File file) {
        throw new RuntimeException("TODO: reading file bytes");
    }

    public static String readFileString(final File file) {
        if (canReadFile(file)) {
            try {
                return readSteamString(new FileInputStream(file));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static String[] readFileLines(final File file, final boolean ignoreComments) {
        if (canReadFile(file)) {
            try {
                return readStreamLines(new FileInputStream(file), ignoreComments);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static String[] readStreamLines(final InputStream is, final boolean ignoreComments) throws IOException {
        List<String> lines = new ArrayList<>();
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        String line;
        while ((line = br.readLine()) != null) {
            line = line.trim();
            if (ignoreComments && (line.isEmpty() || line.startsWith("#") || line.startsWith("//"))) {
                LOG.debug("Ignored line [{}]", line);
            } else {
                lines.add(line);
            }
        }

        return (String[]) lines.toArray();
    }

    public static String readSteamString(final InputStream is) throws IOException {
        StringBuilder sb = new StringBuilder();
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        String line;
        while ((line = br.readLine()) != null) {
            sb.append(line);
        }
        return sb.toString();
    }

    //========================================================//
    //  file write
    //========================================================//

    public static boolean touchFile(final File targetFile) {

        //TODO: as linux, create if not exists. // ASK: want to also update modified times etc?
        throw new RuntimeException("touch file");

        //        if (targetFile == null) {
        //            mLog.warn("supplied targetFile null, nothing to fondle, er, touch...");
        //        } else {
        //            return saveFile(targetFile, "", false);
        //        }

    }

    private static boolean saveFile(final File targetFile,
                                    final OutputStream output,
                                    final boolean overwriteIfExists) {

        if (targetFile == null) {
            LOG.error("supplied target File is null. save failed!");
            return false;
        } else if (targetFile.exists()) {
            LOG.error("supplied target File exists, not overwriting. save of [{}] failed!", path(targetFile));
            LOG.warn("TODO: want to offer toggle overwrite, would have been {}", overwriteIfExists);
            return false;
        } else if (output == null) {
            LOG.error("supplied output is null. save of [{}] failed!", targetFile.getAbsolutePath());
        }

        return false;
    }

    /**
     * Save a String into the targetFile supplied.
     * Empty String is valid.
     *
     * @param targetFile   The destination for the String
     * @param fileContents Whether to replace the File if it already exists
     * @return success
     */
    public static boolean saveFile(final File targetFile,
                                   final String fileContents,
                                   final boolean overwriteIfExists) {

        // test
        if (fileContents == null) {
            // empty string is valid
            LOG.error("supplied String for fileContents null. save of [{}] failed!", path(targetFile));
            return false;
        } else if (folderExists(targetFile)) {
            LOG.error("that's a;ready a folder! not overwriting [{}]", path(targetFile));
            return false;
        } else if (fileExists(targetFile) && !overwriteIfExists) {
            LOG.warn("File [{}] already exists & overwrite off. Not saving String length [{}]", path(targetFile), fileContents.length());
            return false;
        }

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(path(targetFile)))) {
            writer.write(fileContents);
            writer.close();
            return true;

        } catch (IOException e) {
            LOG.error("failed to save file [{}] with contents of length [{}]", path(targetFile), fileContents.length());
            e.printStackTrace();
            return false;
        }
    }

    ////////////////////////////////////////////////////////////
    ////    FOLDERS
    ////////////////////////////////////////////////////////////

    //========================================================//
    //  folder test/validation
    //  validation chains so calling both canRead and exists separately is redundant
    //========================================================//


    public static boolean folderExists(final File folder) {
        return folder != null
                && folder.exists()
                && folder.isDirectory();
    }

    public static boolean canReadFolder(final File folder) {
        return folderExists(folder)
                && folder.canRead();
    }

    public static boolean canWriteFolder(final File folder) {
        return folderExists(folder)
                && folder.canWrite();
    }

    //========================================================//
    //  folder read
    //========================================================//

    public static List<File> getFiles(final File folder) {

        if (canReadFolder(folder)) {
            File[] fileList = folder.listFiles();
            if (fileList != null) {
                return Arrays.asList(fileList);
            }
        }
        return new ArrayList<>(0);
    }

    //========================================================//
    //  folder create
    //========================================================//

    public static boolean createFolder(final File folder) {
        if (folder == null) {
            LOG.debug("Supplied folder null.");
            return false;
        } else if (!canWriteFolder(folder.getParentFile())) {
            LOG.error("Supplied folder null or parent not writable [{}]", folder.getParentFile().getAbsolutePath());
            return false;
        } else {
            LOG.info("Creating folder [{}]...", folder.getAbsolutePath());
            return folder.mkdir();
        }
    }

    /**
     * @param folder File representing path to create
     * @return true if folder(s) created or already exists
     */
    public static boolean createFolders(final File folder) {
        if (folder == null) {
            LOG.error("Supplied folder null.");
            return false;
        } else if (folderExists(folder)) {
            LOG.info("Folder [{}] already exists.", path(folder));
            return true; //ASK: ok with true for exists and not created?
        } else {
            try {
                // will return false if exists, only true if created ALL needed, may return false but leave some parent folders behind
                return folder.mkdirs();
            } catch (SecurityException e) {
                LOG.warn("Do not have permission to create [{}]", path(folder));
                return false;
            }
        }
    }

    public static File createTempDir(final String dirName,
                                     final boolean deleteOnShutdown /*TODO*/)
            throws IllegalArgumentException {

        if (Nulls.isNullOrEmptyWithTrim(dirName)) {
            LOG.error("Invalid tmpJavaDir name supplied [{}}", dirName);
            throw new IllegalArgumentException("a valid 'dirName' must be supplied (non null, non empty String)");
        }

        Path tempDirPath;
        try {
            tempDirPath = Files.createTempDirectory(dirName.trim());
            if (tempDirPath != null && tempDirPath.toUri() != null) {
                LOG.info("Created temp dir at: {}", tempDirPath.toUri());
                File dir = new File(tempDirPath.toUri().toString());
                dir.setWritable(true);
                if (!FileUtils.canWriteFolder(dir)) LOG.warn("UNABLE TO WRITE TEMP DIR: " + dir.getAbsolutePath());
                return dir;
            } else {
                LOG.warn("Failed to create temp dir. Path null? [{}] Uri? [{}]",
                        (tempDirPath == null),
                        (tempDirPath == null ? "-" : tempDirPath.toUri() == null));
                return null;
            }

        } catch (SecurityException e) {
            LOG.error("SecEx creating temp dir wih prefix [{}]. Ex = {}", dirName, e.getMessage());
            return null;
        } catch (IOException e) {
            LOG.error("IOEx creating temp dir with prefix [{}]. Ex = {}", dirName, e.getMessage());
            return null;
        }
    }


    ////////////////////////////////////////////////////////////
    ////    util
    ////////////////////////////////////////////////////////////

    public static String path(final File fileOrFolder) {
        return (fileOrFolder == null) ? "null" : fileOrFolder.getAbsolutePath();
    }

}
