package wtf.indivisible.utils.id;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.UUID;
import java.util.regex.Pattern;

/**
 * Generates and validates UUID Strings
 * <p>
 * A UUID is a String of hex length 36 and with 4 seps:
 * "[8 Hex]-[4 Hex]-[4 Hex]-[4 Hex]-[12 Hex]".
 */
public class UUIDHelper implements IdentityHelper {

    private static final Logger LOG = LoggerFactory.getLogger(UUIDHelper.class);

    ////////////////////////////////////////////////////////////////////////////
    ////  validation
    ////////////////////////////////////////////////////////////////////////////

    /**
     * Placeholder value for none or no Id while still being considered valid
     */
    public static final String NONE_ID = "00000000-0000-0000-0000-000000000000";
    public static final int VALID_UUID_LENGTH = 36;
    private static final String REGEX_UUID = "^" +
                                             "[a-fA-F0-9]{8}-" +
                                             "[a-fA-F0-9]{4}-" +
                                             "[a-fA-F0-9]{4}-" +
                                             "[a-fA-F0-9]{4}-" +
                                             "[a-fA-F0-9]{12}$";
    private static final Pattern PATTERN_UUID = Pattern.compile(REGEX_UUID);

    /**
     * Validates a String Id for length and format.
     * Upper and lower cases are allowed (though only lowercase values are ever generated).
     */
    public boolean isValidId(final String id) {
        if (id == null) {
            LOG.trace("Invalid Id supplied. (NULL)");
            return false;
        } else if (isNoneId(id)) {
            return true;
        } else if (id.length() != VALID_UUID_LENGTH) {
            LOG.trace("Invalid Id supplied (BadLength: {})", id.length());
            return false;
        } else {
            return PATTERN_UUID.matcher(id).matches();
        }
    }

    /**
     * Test whether the supplied value is equal to the sentinel "none" value ({@value UUIDHelper#NONE_ID)
     *
     * @param id to test for "none"
     * @return
     */
    public boolean isNoneId(final String id) {
        // no need to convert case as "none id" is only zeros
        return NONE_ID.equals(id);
    }


    ////////////////////////////////////////////////////////////////////////////
    ////  generation
    ////////////////////////////////////////////////////////////////////////////

    @Override
    public String newId() {
        return UUID.randomUUID().toString();
    }

}
