package wtf.indivisible.utils.id;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import wtf.indivisible.utils.nulls.Nulls;

import java.util.Locale;

///**
// * Generates and validates Ids.
// * An Id is a UUID String of length 36, "[8 Hex]-[4 Hex]-[4 Hex]-[4 Hex]-[12 Hex]".
// * <p>
// * Either upper- or lower-case values are valid inputs but only lower-case values are generated.
// * <p>
// * Empty values supplied for ids may be treated as NULLs or as NONE depending on the implementation.
// */

/**
 * Generic methods to help generating and validating Identity Strings.
 *
 * @see IdentityHelper
 * @see UUIDHelper
 * @see MinIdHelper
 */
public class IdUtils {

    private static final Logger LOG = LoggerFactory.getLogger(IdUtils.class);


    ////////////////////////////////////////////////////////////////////////////
    ////  generic helper methods
    ////////////////////////////////////////////////////////////////////////////

    /**
     * Sanitise a String Id. Trims whitespace and converts to lowercase
     * A null or invalid format Id returns null.
     */
    public static String cleanId(final String id, final boolean doTrim, final boolean doConvertCase) {
        if (Nulls.isNullOrEmptyWithTrim(id)) {
            return null;    //NB: empty/blank considered NULL
        } else {
            String out = id;
            if (doTrim) out = out.trim();
            if (doConvertCase) out = out.toLowerCase(Locale.ENGLISH);
            return out;
        }
    }


    /**
     * Equate two Ids, no case manipulation is performed but as such differing cases will not equal. <br>
     * Assumes any cleaning/trimming/sanitization has already been done.
     * <p>
     * Does NOT perform any format/content validation of inputs, only a simple String equality check.
     */
    public static boolean equalIds(final String id1, final String id2) {
        return equalIds(id1, id2, false, false);
    }

    /**
     * Equate two Ids optionally sanitising the inputs (trim & case). <br>
     * This method can handle mixed/differing case inputs when supplied with (doConvertCase:true) <br>
     * and can also trim whitespace before equating with (cleanIds:true)
     *
     * @param doTrimIds     true will trim both inputs (trim,empty->null)
     * @param doConvertCase true will convert/force ids to lowercase before comparison
     * @see IdUtils#cleanId(String, boolean, boolean)
     */
    public static boolean equalIds(final String id1, final String id2,
                                   final boolean doTrimIds, final boolean doConvertCase) {
        String clean1 = cleanId(id1, doTrimIds, doConvertCase);
        String clean2 = cleanId(id2, doTrimIds, doConvertCase);
        return (clean1 != null) && clean1.equals(clean2);

    }

    /**
     * Compare two {@link Identifiable} instances by their Ids.
     * <p>
     * Does NOT perform any sanitisation on the input Ids, any cleaning is
     * assumed to have already been performed.
     *
     * @param o1
     * @param o2
     * @return
     */
    public static boolean equalIds(final Identifiable o1, final Identifiable o2) {
        if (o1 == null || o2 == null) {
            return false;
        } else {
            return equalIds(o1.getId(), o2.getId(), false, false);
        }
    }


}
