package wtf.indivisible.utils.id;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Base64;
import java.util.Random;
import java.util.regex.Pattern;

public class MinIdHelper implements IdentityHelper {

    private static final Logger LOG = LoggerFactory.getLogger(MinIdHelper.class);
    private static final Random RAND = new Random();

    public static final int DEFAULT_LENGTH = 16;
    public static final int MAX_LENGTH = 128;

    public static final String NONE_ID = "-";
    public static final String PATTERN_MINID = "^[+\\/a-zA-Z0-9]*$"; //TODO <----
    private static final Pattern REGEX_MINID = Pattern.compile(PATTERN_MINID);


    ////////////////////////////////////////////////////////////////////////////
    ////  validation
    ////////////////////////////////////////////////////////////////////////////


    @Override
    public boolean isValidId(final String id) {
        if (id == null) {
            LOG.trace("Supplied invalid id: null");
            return false;
        } else if (isNoneId(id)) {
            // "none" is a valid value
            return true;
        } else if (id.isBlank()) {
            LOG.trace("Supplied invalid id: blank");
            return false;
        } else {
            return REGEX_MINID.matcher(id).matches();
        }
    }

    /**
     * Test whether the value supplied is equal to the sentinel "none" value ({@value })
     *
     * @param id
     * @return
     * @see MinIdHelper#NONE_ID NONE_ID
     */
    @Override
    public boolean isNoneId(final String id) {
        return NONE_ID.equals(id);
    }

    /**
     * To sidestep the padding/rounding char issue of Base64 encoding
     * only lengths of multiples of 4 are allowed.
     *
     * @param length the length of id being requested
     * @throws IllegalArgumentException
     */
    private static void ensureValidLengthId(final int length) throws IllegalArgumentException {
        if (length <= 0) {
            LOG.info("Invalid MinId length generation request (length negative/zero): {}", length);
            throw new IllegalArgumentException("Invalid MinId length request (negative/zero)");
        } else if ((length % 4) != 0) {
            LOG.info("Invalid MinId length generation request (not multiple of 4): {}", length);
            throw new IllegalArgumentException("Invalid MinId length request (not multiple of 4)");
        } else if (length > MAX_LENGTH) {
            LOG.info("Invalid MinId length generation request (higher than max {}): {}", MAX_LENGTH, length);
            throw new IllegalArgumentException("Invalid MinId length request (too long)");
        }

        // nada, valid length
    }

    ////////////////////////////////////////////////////////////////////////////
    ////  generation
    ////////////////////////////////////////////////////////////////////////////

    /**
     * Generate a new "MinId" using the default length ({@value MinIdHelper#DEFAULT_LENGTH})
     *
     * @return new String id (base64 encoded)
     * @see MinIdHelper#DEFAULT_LENGTH
     */
    @Override
    public String newId() {
        //REM: this MUST supply a good default length to never trigger an IllegalArgException
        return newId(MinIdHelper.DEFAULT_LENGTH);
    }

    /**
     * Generate a new, random "MinId" of the requested length.
     * <p>
     * The length MUST be divisible by 4 to side-step Base64 encoding padding issues. <br>
     * This restriction _may_ change later but not a big enough issue to bother with now.
     *
     * @param idLength desired length of id to generate
     * @return a random base64 encoded id of the requested length
     * @throws IllegalArgumentException on invalid length
     * @see MinIdHelper#ensureValidLengthId(int)
     */
    public String newId(final int idLength) throws IllegalArgumentException {
        ensureValidLengthId(idLength);

        // Base64 encoding: 3 bytes --> 4 chars
        int numBytesForId = (idLength / 4) * 3;
        byte[] randomBytes = new byte[numBytesForId];
        RAND.nextBytes(randomBytes);

        return Base64.getEncoder().encodeToString(randomBytes);
    }


}
