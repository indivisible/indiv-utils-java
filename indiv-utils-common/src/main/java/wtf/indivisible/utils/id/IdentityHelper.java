package wtf.indivisible.utils.id;

public interface IdentityHelper {

    String newId();

    boolean isValidId(final String id);

    boolean isNoneId(final String id);

}
