package wtf.indivisible.utils.id;

import wtf.indivisible.utils.nulls.Nulls;

public interface Identifiable {

    String getId();

    default boolean hasId() {
        return Nulls.notNullOrEmptyWithTrim(getId());
    }

    static boolean hasId(final Identifiable id) {
        return id != null && id.hasId();
    }

    default boolean equalsById(final String id) {
        return IdUtils.equalIds(getId(), id);
    }

    default boolean equalsById(final Identifiable o) {
        if (o == null) return false;
        else return IdUtils.equalIds(getId(), o.getId());
    }

}
