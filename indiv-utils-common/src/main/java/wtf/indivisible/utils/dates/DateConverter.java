package wtf.indivisible.utils.dates;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.*;
import java.util.Date;

/**
 * Common, generic Date conversion.
 * <br>
 * Supports conversion between: <ul>
 *   <li>Long (epoch milliseconds),</li>
 *   <li>java.util.Date,</li>
 *   <li>java.time.Instant,</li>
 *   <li>java.time.LocalDate,</li>
 *   <li>java.time.LocalDateTime</li>
 *  </ul>
 */
public class DateConverter {

    private static final Logger LOG = LoggerFactory.getLogger(DateConverter.class);


    //// to Long (epoch milliseconds)

    public static Long toEpochMillis(final Date date) {
        if (date == null) {
            LOG.debug("Supplied java.util.Date is NULL");
            return null;
        } else {
            return date.getTime();
        }
    }

    public static Long toEpochMillis(final Instant instant) {
        if (instant == null) {
            LOG.debug("Supplied java.time.Instant is NULL");
            return null;
        } else {
            return instant.getEpochSecond();
        }
    }

    public static Long toEpochMillis(final LocalDate localDate, final ZoneId zoneId) {
        if (localDate == null) {
            LOG.debug("Supplied java.time.LocalDate is NULL");
            return null;
        } else if (zoneId == null) {
            LOG.debug("Supplied java.time.ZoneId is NULL");
            return null;
        } else {
            LocalDateTime ldt = localDate.atStartOfDay();
            return ldt.atZone(zoneId).toEpochSecond() * 1000L;
        }
    }

    public static Long toEpochMillis(final LocalDate localDate) {
        // assume and use local/system timezone
        return toEpochMillis(localDate, ZoneId.systemDefault());
    }

    public static Long toEpochMillis(final LocalDateTime localDateTime, final ZoneId zoneId) {
        if (localDateTime == null) {
            LOG.debug("Supplied java.time.LocalDateTime is NULL");
            return null;
        } else if (zoneId == null) {
            LOG.debug("Supplied java.time.ZoneId is NULL");
            return null;
        } else {
            return localDateTime.atZone(zoneId).toEpochSecond() * 1000L;
        }
    }

    public static Long toEpochMillis(final LocalDateTime localDateTime) {
        // assume and use local/system timezone
        return toEpochMillis(localDateTime, ZoneId.systemDefault());
    }


    //// to java.util.Date

    public static Date toDate(final Long epochMillis) {
        if (epochMillis == null) {
            LOG.debug("Supplied Long (epoch milliseconds) is NULL");
            return null;
        } else {
            return new Date(epochMillis);
        }
    }

    public static Date toDate(final Instant instant) {
        if (instant == null) {
            LOG.debug("Supplied java.time.Instant is NULL");
            return null;
        } else {
            return new Date(instant.toEpochMilli());
        }
    }

    public static Date toDate(final LocalDate localDate, final ZoneId zoneId) {
        if (localDate == null) {
            LOG.debug("Supplied java.time.LocalDate is NULL");
            return null;
        } else if (zoneId == null) {
            LOG.debug("Supplied java.time.ZoneId is NULL");
            return null;
        } else {
            return new Date(localDate.atStartOfDay(zoneId).toEpochSecond() * 1000L);
        }
    }

    public static Date toDate(final LocalDate localDate) {
        return toDate(localDate, ZoneId.systemDefault());
    }

    public static Date toDate(final LocalDateTime localDateTime, final ZoneId zoneId) {
        if (localDateTime == null) {
            LOG.debug("Supplied java.time.LocalDateTime is NULL");
            return null;
        } else if (zoneId == null) {
            LOG.debug("Supplied java.time.ZoneId is NULL");
            return null;
        } else {
            return new Date(localDateTime.atZone(zoneId).toEpochSecond() * 1000L);
        }
    }

    public static Date toDate(final LocalDateTime localDateTime) {
        return toDate(localDateTime, ZoneId.systemDefault());
    }


    //// to java.time.Instant

    public static Instant toInstant(final Long epochMillis) {
        if (epochMillis == null) {
            LOG.debug("Supplied Long (epoch milliseconds) is NULL");
            return null;
        } else {
            return Instant.ofEpochMilli(epochMillis);
        }
    }

    public static Instant toInstant(final Date date) {
        if (date == null) {
            LOG.debug("Supplied java.util.Date is NULL");
            return null;
        } else {
            return Instant.ofEpochMilli(date.getTime());
        }
    }

    public static Instant toInstant(final LocalDate localDate, final ZoneId zoneId) {
        if (localDate == null) {
            LOG.debug("Supplied java.time.LocalDate is NULL");
            return null;
        } else if (zoneId == null) {
            LOG.debug("Supplied java.time.ZoneId is NULL");
            return null;
        } else {
            return localDate.atStartOfDay(zoneId).toInstant();
        }
    }

    public static Instant toInstant(final LocalDate localDate) {
        return toInstant(localDate, ZoneId.systemDefault());
    }

    public static Instant toInstant(final LocalDateTime localDateTime, final ZoneId zoneId) {
        if (localDateTime == null) {
            LOG.debug("Supplied java.time.LocalDateTime is NULL");
            return null;
        } else if (zoneId == null) {
            LOG.debug("Supplied java.time.ZoneId is NULL");
            return null;
        } else {
            return localDateTime.atZone(zoneId).toInstant();
        }
    }

    public static Instant toInstant(final LocalDateTime localDateTime) {
        return toInstant(localDateTime, ZoneId.systemDefault());
    }


    //// to java.time.LocalDate

    public static LocalDate toLocalDate(final Long epochMillis) {
        if (epochMillis == null) {
            LOG.debug("Supplied Long (epoch milliseconds) is NULL");
            return null;
        } else {
            return LocalDate.ofInstant(Instant.ofEpochMilli(epochMillis), ZoneOffset.UTC);
        }
    }

    public static LocalDate toLocalDate(final Date date) {
        if (date == null) {
            LOG.debug("Supplied java.util.Date is NULL");
            return null;
        } else {
            return LocalDate.ofInstant(Instant.ofEpochMilli(date.getTime()), ZoneOffset.UTC);
        }
    }

    public static LocalDate toLocalDate(final Instant instant) {
        if (instant == null) {
            LOG.debug("Supplied java.time.Instant is NULL");
            return null;
        } else {
            return LocalDate.ofInstant(instant, ZoneOffset.UTC);
        }
    }

    public static LocalDate toLocalDate(final LocalDateTime localDateTime) {
        if (localDateTime == null) {
            LOG.debug("Supplied java.time.LocalDateTime is NULL");
            return null;
        } else {
            return LocalDate.from(localDateTime);
        }
    }


    //// to java.time.LocalDateTime

    public static LocalDateTime toLocalDateTime(final Long epochMillis) {
        if (epochMillis == null) {
            LOG.debug("Supplied Long (epoch milliseconds) is NULL");
            return null;
        } else {
            return LocalDateTime.ofInstant(Instant.ofEpochMilli(epochMillis), ZoneOffset.UTC);
        }
    }

    public static LocalDateTime toLocalDateTime(final Date date) {
        if (date == null) {
            LOG.debug("Supplied java.util.Date is NULL");
            return null;
        } else {
            return LocalDateTime.ofInstant(Instant.ofEpochMilli(date.getTime()), ZoneOffset.UTC);
        }
    }

    public static LocalDateTime toLocalDateTime(final Instant instant) {
        if (instant == null) {
            LOG.debug("Supplied java.time.Instant is NULL");
            return null;
        } else {
            return LocalDateTime.ofInstant(instant, ZoneOffset.UTC);
        }
    }

    public static LocalDateTime toLocalDateTime(final LocalDate localDate) {
        if (localDate == null) {
            LOG.debug("Supplied java.time.LocalDate is NULL");
            return null;
        } else {
            return localDate.atStartOfDay();
        }
    }

}
