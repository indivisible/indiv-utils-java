package wtf.indivisible.utils.dates;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public enum DateFormats {

    ISO_DATE("yyyy-MM-dd"),
    ISO_DATETIME("yyyy-MM-dd'T'HH:mm:ss"),

    EU_DATE_LONG("E dd MMM yyyy"),
    EU_DATE_SHORT("dd-MM-yyyy"),
    EU_DATETIME_LONG("E dd MMM yyyy HH:mm:ss"),
    EU_DATETIME_SHORT("dd-MM-yyyy HH:mm"),

    US_DATE_LONG("E MMM dd yyyy"),
    US_DATE_SHORT("MM-dd-yyyy"),
    US_DATETIME_LONG("E MMM dd yyyy HH:mm:ss"),
    US_DATETIME_SHORT("MM-dd-yyyy HH:mm"),

    ;//end

    private static final Logger LOG = LoggerFactory.getLogger(DateFormats.class);
    private final String mFormatString;

    DateFormats(final String formatString) {
        mFormatString = formatString;
    }


    public String getFormatString() {
        return mFormatString;
    }

    public DateTimeFormatter getFormatter(final ZoneId zoneId) {
        if (zoneId == null) {
            LOG.debug("No java.time.ZoneId supplied, using system zone");
            return DateTimeFormatter.ofPattern(getFormatString()).withZone(ZoneId.systemDefault());
        } else {
            return DateTimeFormatter.ofPattern(getFormatString()).withZone(zoneId);
        }
    }


    //// formats

    public String format(final Long epochMillis, final ZoneId zoneId) {
        Instant i = DateConverter.toInstant(epochMillis);
        if (i == null) {
            LOG.warn("Supplied Long (epoch milliseconds) NULL");
            return null;
        } else if (zoneId == null) {
            LOG.warn("Supplied java.time.ZoneId NULL");
            return null;
        } else {
            return getFormatter(zoneId).format(i);
        }
    }

    public String format(final Long epochMillis) {
        return format(epochMillis, ZoneId.systemDefault());
    }

    public String format(final Date date, final ZoneId zoneId) {
        Instant i = DateConverter.toInstant(date);
        if (i == null) {
            LOG.warn("Supplied java.util.Date NULL");
            return null;
        } else if (zoneId == null) {
            LOG.warn("Supplied java.time.ZoneId NULL");
            return null;
        } else {
            return getFormatter(zoneId).format(i);
        }
    }

    public String format(final Date date) {
        return format(date, ZoneId.systemDefault());
    }

    public String format(final Instant instant, final ZoneId zoneId) {
        if (instant == null) {
            LOG.warn("Supplied java.time.Instant NULL");
            return null;
        } else if (zoneId == null) {
            LOG.warn("Supplied java.time.ZoneId NULL");
            return null;
        } else {
            return getFormatter(zoneId).format(instant);
        }
    }

    public String format(final Instant instant) {
        return format(instant, ZoneId.systemDefault());
    }

    public String format(final LocalDate localDate, final ZoneId zoneId) {
        if (localDate == null) {
            LOG.warn("Supplied java.time.LocalDate NULL");
            return null;
        } else if (zoneId == null) {
            LOG.warn("Supplied java.time.ZoneId NULL");
            return null;
        } else {
            LocalDateTime ldt = localDate.atStartOfDay();
            return getFormatter(zoneId).format(ldt);
        }
    }

    public String format(final LocalDate localDate) {
        return format(localDate, ZoneId.systemDefault());
    }

    public String format(final LocalDateTime localDateTime, final ZoneId zoneId) {
        if (localDateTime == null) {
            LOG.warn("Supplied java.time.LocalDateTime NULL");
            return null;
        } else if (zoneId == null) {
            LOG.warn("Supplied java.time.ZoneId NULL");
            return null;
        } else {
            return getFormatter(zoneId).format(localDateTime);
        }
    }

    public String format(final LocalDateTime localDateTime) {
        return format(localDateTime, ZoneId.systemDefault());
    }

}
