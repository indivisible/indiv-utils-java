package wtf.indivisible.utils.constants;

public class CompareReturns {

    // for THIS.compareTo(THAT)
    public static final int THIS_EQ_THAT = 0;
    public static final int THIS_LT_THAT = -1;
    public static final int THIS_GT_THAT = 1;

    // for Comparator.compare(FIRST, SECOND)
    public static final int FIRST_EQ_SECOND = 0;
    public static final int FIRST_LT_SECOND = -1;
    public static final int FIRST_GT_SECOND = 1;

}
