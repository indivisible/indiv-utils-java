package wtf.indivisible.utils.constants;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

/**
 * Class to aid in reading true/false values in various formats.
 */
public class Truthy {

    public static final String[] TRUTHY_VALUES = {"true", "t", "1", "y", "yes"};
    public static final String[] FALSY_VALUES = {"false", "f", "0", "n", "no"};

    private static final Set<String> TRUTHY_SET = new HashSet<>(Arrays.asList(TRUTHY_VALUES));
    private static final Set<String> FALSY_SET = new HashSet<>(Arrays.asList(FALSY_VALUES));


    public static boolean isTruthy(final String val) {
        return val != null && TRUTHY_SET.contains(val.toLowerCase(Locale.ENGLISH));
    }

    public static boolean isFalsy(final String val) {
        return val != null && FALSY_SET.contains(val.toLowerCase(Locale.ENGLISH));
    }


    public static Boolean parse(final String val) {
        if (isTruthy(val)) {
            return true;
        } else if (isFalsy(val)) {
            return false;
        } else {
            // neither truthy or falsy
            return null;
        }
    }
}
