package wtf.indivisible.utils.props;

/**
 * Class to hold all the JVM supplied/set available system properties. <br>
 * All values are non-modifiable and expected to be "static final"
 * (with the exception of {@value JvmProps#KEY_USER_CWD} which may change frequently)
 * <p>
 * <table>
 *  <th>key</th> <th>summary</th>
 *
 *  <tr><td>{@value JvmProps#KEY_JVM_CLASSPATH}</td>
 *      <td>Path used to find directories and JAR archives containing class files. Elements of the class path are separated by a platform-specific character specified in the path.separator property.</td></tr>
 *  <tr><td>{@value JvmProps#KEY_JVM_HOME}</td>
 *      <td>Installation directory for Java Runtime Environment (JRE)</td></tr>
 *  <tr><td>{@value JvmProps#KEY_JVM_VERSION}</td>
 *      <td>JRE version number</td></tr>
 *  <tr><td>{@value JvmProps#KEY_JVM_VENDOR}</td>
 *      <td>JRE vendor's name</td></tr>
 *  <tr><td>{@value JvmProps#KEY_JVM_VENDOR_URL}</td>
 *      <td>JRE vendor's website URL</td></tr>
 *  <tr></tr>
 *  <tr><td>{@value JvmProps#KEY_OS_ARCH}</td>
 *      <td>Operating system architecture</td></tr>
 *  <tr><td>{@value JvmProps#KEY_OS_NAME}</td>
 *      <td>Operating system name</td></tr>
 *  <tr><td>{@value JvmProps#KEY_OS_VERSION}</td>
 *      <td>Operating system version</td></tr>
 *  <tr></tr>
 *  <tr><td>{@value JvmProps#KEY_OS_SEP_FILE}</td>
 *      <td>Character that separates components of a file path. ("/" on *nix and "\" on Windows)</td></tr>
 *  <tr><td>{@value JvmProps#KEY_OS_SEP_LINE}</td>
 *      <td>Sequence used by operating system to separate lines in text files. ("\n", "\r\n" etc)</td></tr>
 *  <tr><td>{@value JvmProps#KEY_OS_SEP_PATH}</td>
 *      <td>Path separator character used in java.class.path (Usually ";")</td></tr>
 *  <tr></tr>
 *  <tr><td>{@value JvmProps#KEY_USER_CWD}</td>
 *      <td>User's current working directory</td></tr>
 *  <tr><td>{@value JvmProps#KEY_USER_HOME}</td>
 *      <td>User's home directory</td></tr>
 *  <tr><td>{@value JvmProps#KEY_USER_NAME}</td>
 *      <td>User's account name</td></tr>
 * </table>
 *
 * @see <a href="https://docs.oracle.com/javase/tutorial/essential/environment/sysprop.html">Oracle Java SysProps</a>
 */
public enum JvmProps {

    JVM_CLASSPATH(JvmProps.KEY_JVM_CLASSPATH),
    JVM_HOME(JvmProps.KEY_JVM_HOME),
    JVM_VERSION(JvmProps.KEY_JVM_VERSION),
    JVM_VENDOR(JvmProps.KEY_JVM_VENDOR),
    JVM_VENDOR_URL(JvmProps.KEY_JVM_VENDOR_URL),

    OS_ARCH(JvmProps.KEY_OS_ARCH),
    OS_NAME(JvmProps.KEY_OS_NAME),
    OS_VERSION(JvmProps.KEY_OS_VERSION),
    OS_SEP_FILE(JvmProps.KEY_OS_SEP_FILE),
    OS_SEP_LINE(JvmProps.KEY_OS_SEP_LINE),
    OS_SEP_PATH(JvmProps.KEY_OS_SEP_PATH),

    USER_CWD(JvmProps.KEY_USER_CWD),
    USER_HOME(JvmProps.KEY_USER_HOME),
    USER_NAME(JvmProps.KEY_USER_NAME),

    //end
    ;

    public static final String NO_VALUE = "-";


    ////////////////////////////////////////////////////////////////////////////
    //  instance

    private final String mKey;

    JvmProps(final String key) {
        mKey = key;
    }


    ////////////////////////////////////////////////////////////////////////////
    //  methods

    public String getKey() {
        return mKey;
    }

    public String getValue() {
        return SysProps.getString(getKey(), JvmProps.NO_VALUE, false);
    }


    ////////////////////////////////////////////////////////////////////////////
    //  keys

    public static final String KEY_JVM_CLASSPATH = "java.class.path";
    public static final String KEY_JVM_HOME = "java.home";
    public static final String KEY_JVM_VERSION = "java.version";
    public static final String KEY_JVM_VENDOR = "java.vendor";
    public static final String KEY_JVM_VENDOR_URL = "java.vendor.url";

    public static final String KEY_OS_ARCH = "os.arch";
    public static final String KEY_OS_NAME = "os.name";
    public static final String KEY_OS_VERSION = "os.version";
    public static final String KEY_OS_SEP_FILE = "file.separator";
    public static final String KEY_OS_SEP_LINE = "line.separator";
    public static final String KEY_OS_SEP_PATH = "path.separator";

    public static final String KEY_USER_CWD = "user.dir";
    public static final String KEY_USER_HOME = "user.home";
    public static final String KEY_USER_NAME = "user.name";

    private static final int LONGEST_KEY_LENGTH = 15;

    public static final String[] ALL_KEYS = {
        KEY_JVM_CLASSPATH,
        KEY_JVM_HOME,
        KEY_JVM_VERSION,
        KEY_JVM_VENDOR,
        KEY_JVM_VENDOR_URL,

        KEY_OS_ARCH,
        KEY_OS_NAME,
        KEY_OS_VERSION,

        KEY_OS_SEP_FILE,
        KEY_OS_SEP_LINE,
        KEY_OS_SEP_PATH,

        KEY_USER_CWD,
        KEY_USER_HOME,
        KEY_USER_NAME
    };


    ////////////////////////////////////////////////////////////////////////////
    //  toString

    private static final String FORMAT_ONE_PROP = "JvmProp[%s]: %s";
    private static final String FORMAT_ALL_PROPS_LINE = "  %-" + LONGEST_KEY_LENGTH + "s  %s";

    @Override
    public String toString() {
        return String.format(FORMAT_ONE_PROP, getKey(), getValue());
    }

    public static String printableProps() {

        StringBuilder sb = new StringBuilder("JVM Props:\n");
        sb.append(String.format(FORMAT_ALL_PROPS_LINE, "Key", "Value"));

        for (JvmProps prop : JvmProps.values()) {
            sb.append(String.format(FORMAT_ALL_PROPS_LINE, prop.getKey(), prop.getValue()));
        }

        return sb.toString();
    }

}
