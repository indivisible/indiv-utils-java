/*
 * Serve
 * Private repo for now, OSS branch later when I like it.
 * indivisible
 */

package wtf.indivisible.utils.props;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import wtf.indivisible.utils.constants.Truthy;
import wtf.indivisible.utils.nulls.Nulls;

import java.util.regex.Pattern;

/**
 * Read/Write access to System Properties
 */
public class SysProps {

    private static final Logger LOG = LoggerFactory.getLogger(SysProps.class);

    // defaults
    public static final boolean DEFAULT_DO_TRIM = false;
    public static final boolean DEFAULT_TEST_KEY_WITH_REGEX = true;
    public static final String DEFAULT_FALLBACK_VALUE = null;


    ////////////////////////////////////////////////////////////
    ////    key validation & cleaning
    ////////////////////////////////////////////////////////////

    private static final String REGEX_KEY = "^[-._a-zA-Z0-9]+?$";
    private static final Pattern PATTERN_KEY = Pattern.compile(REGEX_KEY);


    public static boolean isValidKey(final String key, final boolean testWithRegex) {
        if (Nulls.isNullOrEmptyWithTrim(key)) {
            LOG.warn("Supplied key is INVALID: null/empty");
            return false;
        } else if (testWithRegex && !PATTERN_KEY.matcher(key).matches()) {
            LOG.warn("Supplied key is INVALID: Failed regex test - '" + key + "'");
            return false;
        } else {
            // non null, non empty, passes regex (if required)
            return true;
        }
    }

    public static boolean isValidKey(final String key) {
        return isValidKey(key, DEFAULT_TEST_KEY_WITH_REGEX);
    }

    private static void assertValidKey(final String key, final boolean testWithRegex) throws IllegalArgumentException {
        if (!isValidKey(key, testWithRegex)) {
            throw new IllegalArgumentException("Key must be non null, non empty and may only contain alphanumerics (a-z, A-Z, 0-9) & chars '.', '-', '_'");
        }
    }

    private static void assertValidKey(final String key) throws IllegalArgumentException {
        assertValidKey(key, DEFAULT_TEST_KEY_WITH_REGEX);
    }


    ////////////////////////////////////////////////////////////
    ////    primitives
    ////////////////////////////////////////////////////////////

    //========================================================//
    //  Most other reads/writes depend on the String methods
    //========================================================//

    /**
     * Get a single String property's value.
     *
     * @param key           property's key
     * @param fallbackValue default value to return on "no value set/found"
     * @param doTrim        whether to trim the discovered value before return (for convenience)
     * @return the discovered property value or fallback/default supplied
     * @throws IllegalArgumentException on an invalid key supplied
     */
    public static String getString(final String key, final String fallbackValue, final boolean doTrim) throws IllegalArgumentException {
        assertValidKey(key);
        final String val = System.getProperty(key, fallbackValue);
        if (val == null) {
            LOG.warn("Retrieved (String) SysProp NULL for key [{}}. Returning default [{}]", key, fallbackValue);
            return fallbackValue;
        } else {
            return (doTrim ? val.trim() : val);
        }
    }

    /**
     * Get a single String property's value.
     *
     * @param key          property's key
     * @param defaultValue default value to return on "no value set/found"
     * @return the discovered property value or fallback/default supplied
     * @throws IllegalArgumentException on an invalid key supplied
     * @see SysProps#getString(String, String, boolean) SysProps.getString(String, String, boolean)
     */
    public static String getString(final String key, final String defaultValue) throws IllegalArgumentException {
        return getString(key, null, false);
    }

    public static String getString(final String key) throws IllegalArgumentException {
        return getString(key, null);
    }

    public static void putString(final String key, final String value) throws IllegalArgumentException, SecurityException {
        assertValidKey(key);
        System.setProperty(key, value);
    }

    public static Boolean getBoolean(final String key, final Boolean defaultValue) throws IllegalArgumentException {
        String strVal = getString(key, null, true);
        Boolean boolVal = Truthy.parse(strVal);
        if (boolVal != null) {
            return boolVal;
        } else {
            LOG.warn("Failed to cast String [{}] to a True/False value", strVal);
            return defaultValue;
        }
    }

    public static Boolean getBoolean(final String key) throws IllegalArgumentException {
        return getBoolean(key, null);
    }

    public static void putBoolean(final String key, final boolean value) throws IllegalArgumentException, SecurityException {
        assertValidKey(key);
        String val = Boolean.toString(value);
        putString(key, val);
    }

    public static Integer getInteger(final String key, final Integer defaultValue) throws IllegalArgumentException {
        String strVal = getString(key, null, true);
        if (strVal == null) return defaultValue;
        else try {
            return Integer.parseInt(strVal);
        } catch (NumberFormatException e) {
            LOG.warn("Failed to cast String [{}] to Integer", strVal);
            return defaultValue;
        }
    }

    public static Integer getInteger(final String key) throws IllegalArgumentException {
        return getInteger(key, null);
    }

    public static void putInteger(final String key, final int value) throws IllegalArgumentException, SecurityException {
        assertValidKey(key);
        String val = Integer.toString(value);
        putString(key, val);
    }

    public static Float getFloat(final String key, final Float defaultValue) throws IllegalArgumentException {
        String strVal = getString(key, null, true);
        if (strVal == null) return defaultValue;
        else try {
            return Float.parseFloat(strVal);
        } catch (NumberFormatException e) {
            LOG.warn("Failed to cast String [{}] to Float", strVal);
            return defaultValue;
        }
    }

    public static Float getFloat(final String key) throws IllegalArgumentException {
        return getFloat(key, null);
    }

    public static void putFloat(final String key, final float value) throws IllegalArgumentException, SecurityException {
        assertValidKey(key);
        String val = Float.toString(value);
        putString(key, val);
    }

    public static Double getDouble(final String key, final Double defaultValue) throws IllegalArgumentException {
        String strVal = getString(key, null, true);
        if (strVal == null) return defaultValue;
        else try {
            return Double.parseDouble(strVal);
        } catch (NumberFormatException e) {
            LOG.warn("Failed to cast String [{}] to Double", strVal);
            return defaultValue;
        }
    }

    public static Double getDouble(final String key) throws IllegalArgumentException {
        return getDouble(key, null);
    }

    public static void putDouble(final String key, final double value) throws IllegalArgumentException, SecurityException {
        assertValidKey(key);
        String val = Double.toString(value);
        putString(key, val);
    }


}
