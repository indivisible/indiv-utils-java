package wtf.indivisible.utils.os;

import wtf.indivisible.utils.generics.Stringable;

import java.util.HashMap;
import java.util.Map;

/**
 * Enum of supported/common Operating Systems.
 * <p>
 * Has shorthand names (fixed length 3 chars) and longhand (variable, human readable)
 */
public enum OS implements Stringable<OS> {

    WIN("win", "Windows"),
    NIX("nix", "Linux/Unix"),
    DROID("and", "Android"),
    OSX("osx", "Mac OSX"),
    IOS("ios", "Apple iOS");


    //// static mapping
    private static final Map<String, OS> SHORT_MAP;
    private static final int FIXED_LENGTH_SHORTHAND = 3;

    static {
        SHORT_MAP = new HashMap<>(OS.values().length);
        for (OS os : OS.values()) {
            SHORT_MAP.put(os.getShorthand(), os);
        }
    }


    //// members & accessors

    private final String mShorthand;
    private final String mLonghand;

    OS(final String shorthand, final String longhand) {
        mShorthand = shorthand;
        mLonghand = longhand;
    }


    public String getShorthand() {
        return mShorthand;
    }

    public String getLonghand() {
        return mLonghand;
    }


    //// implements

    @Override
    public OS fromString(final String shorthand) {
        if (shorthand == null || shorthand.length() != FIXED_LENGTH_SHORTHAND) {
            return null;
        } else {
            return SHORT_MAP.get(shorthand);
        }
    }

    @Override
    public String asString() {
        return getShorthand();
    }

    @Override
    public String toString() {
        return getLonghand();
    }


}
